<?php

namespace FOPG\Component\MOFBundle\Tests;

use FOPG\Component\MOFBundle\Contracts\Unity\UnityAttributeInterface;
use FOPG\Component\MOFBundle\Serializer\XmlSerializer;
use FOPG\Component\MOFBundle\Serializer\DtdSerializer;
use FOPG\Component\UtilsBundle\Test\TestCase;
use FOPG\Component\UtilsBundle\Test\TestGiven;
use FOPG\Component\MOFBundle\Serializer\Response\JsonResponse;

class XmlSerializerTest extends TestCase
{
    const SECTION_HEADER = '[Serializer:XmlSerializer]';

    public function testRetrieveData(): void {

      $this->section(self::SECTION_HEADER.' Extraction des données d\'un XML');

      /** @var string $directory Répertoire de stockage des XML */
      $directory = __DIR__.'/docs/serializer/xml_serializer';

      $this
        ->given(
          description: "Contrôle de la récupération des informations du XML",
          directory: $directory
        )
        ->when(
          description: "Je récupére le XML 'doc.xml' d'un répertoire",
          callback: function(string $directory, ?array &$files=[], ?XmlSerializer &$xml=null) {
            $files = XmlSerializer::getFiles($directory);
            if(count($files) > 0)
              foreach($files as $file)
                if(preg_match("/doc[.]xml$/", $file))
                  $xml = new XmlSerializer($file);
          }
        )
        ->andWhen(
          description: "Je souhaite analyser un XML 'complexe'",
          callback: function(string $directory, array $files, ?string &$tmpFile=null, ?XmlSerializer &$natinfXml=null) {
            if(count($files) > 0)
              foreach($files as $file)
                if(preg_match("/domnatinf/", $file)) {
                  $tmpFile = XmlSerializer::generate_dtd_from_xml($file);
                  $natinfXml = new XmlSerializer($file);
                }
          }
        )
        ->then(
          description: "Je dois avoir une DTD constituée",
          callback: function(?string $tmpFile) {
            /** @var bool $check */
            $check = false;
            if(false !== ($data = @file_get_contents($tmpFile))) {
              $elements = explode("\r\n", $data);
              foreach($elements as $element) {
                if(preg_match("/element version /i", $element)) {
                  $check =
                    (bool)preg_match("/[(,]DateFinApplication[?][,)]/", $element) &&
                    (bool)preg_match("/[[(,]NatureFaute[,)]/", $element)
                  ;
                }
              }
            }
            return $check;
          },
          result: true
        )
        ->andThen(
          description: "Le contenu de la DTD générée doit être analysable via Unity",
          callback: function(?string $tmpFile) {
            $dtd = new DtdSerializer($tmpFile);
            /** @var UnityClass $root */
            $root = $dtd->getRoot();
            /** @var ?UnityClass $natinf */
            $natinf = $root->findClass('Natinf');
            $natinfArr = $natinf ? $natinf->toArray() : [];
            /** @var ?string $var1 Validation d'un attribut */
            $var1 = $natinfArr['attributes']['numeroNatinf']['type'] ?? null;
            /** @var ?string $var2 Validation d'un parcours de noeud */
            $var2 = $natinfArr['associations']['AnnulationNatinf']['class']['attributes']['dateAnnulation']['type'] ?? null;
            return
              (UnityAttributeInterface::TYPE_STRING === $var1) &&
              (UnityAttributeInterface::TYPE_STRING === $var2)
            ;
          },
          result: true
        )
        ->andWhen(
          description: "Je souhaite trouver les types d'attributs de classes à partir des données de l'XML",
          callback: function(?string $tmpFile, XmlSerializer $natinfXml, ?DtdSerializer &$dtd=null) {
            $dtd = new DtdSerializer($tmpFile);
            XmlSerializer::guess_types($natinfXml, $dtd);
          }
        )
        ->andThen(
          description: "La découverte des types à partir de l'XML doit être effective",
          callback: function(DtdSerializer $dtd) {
            /** @var UnityClass $datesGestion */
            $datesGestion = $dtd->getRoot()->findClass('DatesGestion');
            /** @var UnityAttribute $dateDemandeMad */
            $dateDemandeMad = $datesGestion->getAttribute('dateDemandeMad');
            return $dateDemandeMad->getType();
          },
          result: UnityAttributeInterface::TYPE_DATE
        )
      ;
    }
}
