<?php

namespace FOPG\Component\MOFBundle\Tests;

use FOPG\Component\MOFBundle\Contracts\Unity\UnityAttributeInterface;
use FOPG\Component\MOFBundle\Serializer\DtdSerializer;
use FOPG\Component\MOFBundle\Serializer\XmlSerializer;
use FOPG\Component\MOFBundle\Serializer\Unity\UnityClass;
use FOPG\Component\UtilsBundle\Env\Env;
use FOPG\Component\UtilsBundle\Filesystem\File;
use FOPG\Component\UtilsBundle\ShellCommand\ShellCommand;
use FOPG\Component\UtilsBundle\Test\TestCase;
use FOPG\Component\UtilsBundle\Test\TestGiven;

class DtdSerializerTest extends TestCase
{
    const SECTION_HEADER = '[Serializer:DtdSerializer]';

    public function testRetrieveData(): void {

      $this->section(self::SECTION_HEADER.' Extraction des données d\'une DTD');

      /** @var string $directory Répertoire de stockage des PDF A3 */
      $directory = __DIR__.'/docs/serializer/dtd_serializer';
      /** @var ?bool $ignoreWIP */
      $ignoreWIP = Env::get("TEST__IGNORE_WIP") ? (bool)Env::get("TEST__IGNORE_WIP") : false;

      $this
        ->given(
          description: "Contrôle de la récupération des informations de la DTD",
          directory: $directory,
          ignoreWIP: $ignoreWIP
        )
        ->when(
          description: "Je récupére la seule DTD d'un répertoire",
          callback: function(string $directory, ?array &$files=[], ?DtdSerializer &$dtd=null) {
            $files = DtdSerializer::getFiles($directory);
            foreach($files as $file)
              if(preg_match("/domnatinf/", $file))
                $dtd = new DtdSerializer($file);
          }
        )
        ->then(
          description: "Au moins un fichier a dû être trouvé",
          callback: function(?array $files) {
            return (count($files)>0);
          },
          result: true,
          onFail: function(?array $files, TestGiven $whoami) {
            /** @var int $count */
            $count = is_array($files) ? count($files) : 0;
            $whoami->addError("$count fichier(s) trouvé(s), 1 attendu",100);
          }
        )
        ->andThen(
          description: "Je dois retrouver les informations de classes/attributs associés",
          callback: function(DtdSerializer $dtd) {
            /** @var UnityClass $root */
            $root = $dtd->getRoot();
            /** @var ?UnityClass $natinf */
            $natinf = $root->findClass('Natinf');
            $natinfArr = $natinf ? $natinf->toArray() : [];
            /** @var ?string $var1 Validation d'un attribut */
            $var1 = $natinfArr['attributes']['numeroNatinf']['type'] ?? null;
            /** @var ?string $var2 Validation d'un parcours de noeud */
            $var2 = $natinfArr['associations']['AnnulationNatinf']['class']['attributes']['dateAnnulation']['type'] ?? null;

            return
              (UnityAttributeInterface::TYPE_STRING === $var1) &&
              (UnityAttributeInterface::TYPE_STRING === $var2)
            ;
          },
          result: true
        )
      ;
    }
}
