<?php

namespace FOPG\Component\MOFBundle\Tests;

use FOPG\Component\MOFBundle\Exception\Unity\CollisionNamespaceException;
use FOPG\Component\MOFBundle\Serializer\Unity\UnityAssociation;
use FOPG\Component\MOFBundle\Serializer\Unity\UnityAssociationEnd;
use FOPG\Component\MOFBundle\Serializer\Unity\UnityAttribute;
use FOPG\Component\MOFBundle\Serializer\Unity\UnityClass;
use FOPG\Component\MOFBundle\Serializer\Unity\UnityNamespace;
use FOPG\Component\UtilsBundle\Env\Env;
use FOPG\Component\UtilsBundle\Test\TestCase;
use FOPG\Component\UtilsBundle\Test\TestGiven;
use FOPG\Component\UtilsBundle\String\StringFacility;

class UnitySerializerTest extends TestCase
{
    const SECTION_HEADER = '[Serializer:UnityClass]';

    public function testCheckAttribute(): void {
        $this->section(self::SECTION_HEADER.' Contrôles liés aux descripteurs d\'attributs de classe');
        /** @var ?bool $ignoreWIP */
        $ignoreWIP = Env::get("TEST__IGNORE_WIP") ? (bool)Env::get("TEST__IGNORE_WIP") : false;
        $badAttributeNamed = "NOmParent";
        $goodAttributeNamed = "nomParent";
        $this
          ->given(
            description: "Contrôle du comportement des attributs d'une classe",
            badAttributeNamed: $badAttributeNamed,
            goodAttributeNamed: $goodAttributeNamed
          )
          ->when(
            description: "Je déclare une nouvelle classe avec un attribut",
            callback: function(
              string $badAttributeNamed,
              ?UnityClass &$myClass=null,
              ?UnityAttribute &$att1=null,
              ?UnityAttribute &$att2=null
            ) {
              /** @var UnityClass $myClass */
              $myClass = new UnityClass("MyClass");
              /** @var UnityAttribute $att1 */
              $att1 = new UnityAttribute($badAttributeNamed, UnityAttribute::TYPE_STRING);
              $att1->addOption('maxlength', 255);
              $myClass->addAttribute($att1);

              $att2 = new UnityAttribute("AttMyClass", UnityAttribute::TYPE_STRING);
              $myClass->addAttribute($att2);
            }
          )
          ->then(
            description: "Le nom de l'attribut doit être normalisé en CamelCase",
            callback: function(UnityClass $myClass, string $goodAttributeNamed) {
              /** @var Collection<int,UnityAttribute> $attributes */
              $attributes = $myClass->getAttributes();
              /** @var UnityAttribute $attribute */
              $attribute = count($attributes) ? $attributes[0] : null;
              foreach($attributes as $attribute)
                if($attribute->getName() == 'nomParent')
                  return $attribute->getName();
              return null;
            },
            result: $goodAttributeNamed
          )
          ->andThen(
            description: "Le type de l'attribut doit être retrouvé",
            callback: function(UnityAttribute $att1) {
              return $att1->getType();
            },
            result: UnityAttribute::TYPE_STRING
          )
          ->andThen(
            description: "Je dois retrouver les options de l'attribut",
            callback: function(UnityAttribute $att1) {
              return $att1->getOption('maxlength');
            },
            result: 255
          )
          ->andThen(
            description: "Je dois renvoyer null sur les options inexistantes de l'attribut",
            callback: function(UnityAttribute $att1) {
              return $att1->getOption('noOption');
            },
            result: null
          )
          ->andThen(
            description: "Je dois retrouver un smalName purgé de la référence à la classe associée",
            callback: function(UnityAttribute $att2) {
              return $att2->getSmallName();
            },
            result: 'att'
          )
        ;

        $this
          ->given(
            description: "Contrôle de la récupération de l'information portant le hashage"
          )
          ->when(
            description: "Je déclare une classe avec certains attributs constituant le hashage",
            callback: function(?UnityClass &$a=null) {
              $a = new UnityClass('ClassA');
              $b = new UnityClass('ClassB');

              UnityAssociation::generateAssociation(
                  lft: $a,
                  rgt: $b,
                  multiplicity: UnityAssociation::MANY_TO_MANY
              );

              $a1 = new UnityAttribute("propA", UnityAttribute::TYPE_STRING);
              $a->addAttribute($a1);
              $a2 = new UnityAttribute("propB", UnityAttribute::TYPE_STRING);
              $a2->setPartOfHash(true);
              $a->addAttribute($a2);
              $a3 = new UnityAttribute("propC", UnityAttribute::TYPE_STRING);
              $a3->setPartOfHash(true);
              $a->addAttribute($a3);

              $associationEnds = $a->getAssociationEnds();
              foreach($associationEnds as $associationEnd) {
                $associationEnd->setPartOfHash(true);
              }
            }
          )
          ->then(
            description: "Je dois retrouver les attributs nécessaires au hash",
            callback: function(UnityClass $a) {
              $output = $a->getPartsOfHash();
              return in_array("propB", $output) && in_array("classBs", $output) && in_array("propC", $output) && !in_array("propA", $output);
            },
            result: true
          )
        ;
    }

    public function testCopy(): void {
      $this->section(self::SECTION_HEADER.' Contrôles liés aux copies Unity');

      $this
        ->given(
          description: "Contrôle sur l'intégrité des recopies d'une classe"
        )
        ->when(
          description: "J'effectue une copie d'une classe Unity",
          callback: function(?UnityClass &$original=null, ?UnityClass &$copy=null) {
            $na = new UnityNamespace('myword');
            $original = new UnityClass('ClassA');
            $original->setNamespace($na);

            $z = new UnityAttribute("propA", UnityAttribute::TYPE_STRING);
            $original->addAttribute($z);

            $b = new UnityClass('ClassB');

            UnityAssociation::generateAssociation(
                lft: $original,
                rgt: $b,
                multiplicity: UnityAssociation::MANY_TO_MANY
            );
            $copyNamespace = UnityNamespace::copy($na);
            $copy = UnityClass::copy($original, $copyNamespace);
          }
        )
        ->then(
          description: "La copie doit être conforme avec copie sur les attributs en complément",
          callback: function(UnityClass $original, UnityClass $copy) {
            return UnityClass::equals($original, $copy);
          },
          result: true
        )
        ->andThen(
          description: "La copie sur les associations ne doit pas s'opérer",
          callback: function(UnityClass $copy) {
            return (count($copy->getAssociationEnds()) === 0);
          },
          result: true
        )
      ;

      $this
        ->given(
          description: "Contrôle sur l'intégrité des recopies d'un attribut"
        )
        ->when(
          description: "J'effectue une copie d'une attribut Unity",
          callback: function(?UnityAttribute &$z=null, ?UnityAttribute &$copy=null) {
            $na = new UnityNamespace('myword');
            $original = new UnityClass('ClassA');
            $original->setNamespace($na);

            $z = new UnityAttribute("propA", UnityAttribute::TYPE_STRING);
            $original->addAttribute($z);

            $b = new UnityClass('ClassB');

            UnityAssociation::generateAssociation(
                lft: $original,
                rgt: $b,
                multiplicity: UnityAssociation::MANY_TO_MANY
            );

            $copy = UnityAttribute::copy($z);
          }
        )
        ->then(
          description: "Les informations de l'attribut doivent être conservés avec perte de la classe d'origine",
          callback: function(UnityAttribute $copy, UnityAttribute $z) {
            return UnityAttribute::equals($copy,$z);
          },
          result: true
        )
      ;

        $this
          ->given(
            description: "Contrôle de la copie d'une association"
          )
          ->when(
            description: "J'effectue une copie de l'association",
            callback: function(
              ?UnityAssociation &$association=null,
              ?UnityAssociation &$copyAssociation=null
            ) {
              $na = self::buildFakeUnityNamespace();
              $lft = $na->findClass('ClassA');
              $rgt = $na->findClass('ClassB');

              /** @var array<int, UnityAssociationEnd> $associationEnds */
              $associationEnds = $lft->getAssociationEnds();
              /** @var UnityAssociation $association */
              $association = $associationEnds[0]->getAssociation();
              /** @var UnityNamespace $copyNamespace */
              $copyNamespace = UnityNamespace::copy($na);
              /** @var UnityClass $copyLft */
              $copyLft = UnityClass::copy($lft, $copyNamespace);
              /** @var UnityClass $copyRgt */
              $copyRgt = UnityClass::copy($rgt, $copyNamespace);
              /** @var UnityAssociation $copyAssociation */
              $copyAssociation = UnityAssociation::copy($association, $copyLft, $copyRgt);
            }
          )
          ->then(
            description: "La copie de l'association doit être conforme",
            callback: function(UnityAssociation $association, UnityAssociation $copyAssociation) {
              return UnityAssociation::equals($association, $copyAssociation);
            },
            result: true
          )
        ;
    }

    private static function buildFakeUnityNamespace(): UnityNamespace {
      $na = new UnityNamespace('myword');
      $lft = new UnityClass('ClassA');
      $lft->setNamespace($na);

      $z = new UnityAttribute("propA", UnityAttribute::TYPE_STRING);
      $z->addOption('theta', 12);
      $lft->addAttribute($z);

      $rgt = new UnityClass('ClassB');

      UnityAssociation::generateAssociation(
          lft: $lft,
          rgt: $rgt,
          multiplicity: UnityAssociation::MANY_TO_MANY
      );
      return $na;
    }

    public static function buildDistinctFakeUnityNamespaces(?UnityNamespace &$a=null, ?UnityNamespace &$b=null): void {
      $a = self::buildFakeUnityNamespace();
      $b = UnityNamespace::duplicate($a);
      $b
        ->findClass('ClassB')
        ->addAttribute(new UnityAttribute('propB', UnityAttribute::TYPE_STRING))
      ;
      $a
        ->findClass('ClassA')
        ->getAttribute('propA')
        ->setType(UnityAttribute::TYPE_TEXT)
      ;

      $b
        ->findClass('ClassA')
        ->getAttribute('propA')
        ->addOption('theta', 15)
      ;

      $b
        ->findClass('ClassA')
        ->getAttribute('propA')
        ->addOption(UnityAttribute::OPTION_STRING_MAXLENGTH, 19)
      ;

      $b
        ->findClass('ClassA')
        ->addAttribute(new UnityAttribute('propC', UnityAttribute::TYPE_STRING))
      ;

      $a
        ->findClass('ClassA')
        ->findAssociation('ClassBClassA')
        ->getAssociationLft()
        ->setMultiplicity(UnityAssociationEnd::ONE_TO_ONE)
      ;

      $b
        ->findClass('ClassA')
        ->findAssociation('ClassBClassA')
        ->getAssociationLft()
        ->setMultiplicity(UnityAssociationEnd::ONE_TO_MANY)
      ;
    }

    public function testMergeNamespace(): void {
      $this->section(self::SECTION_HEADER.' Contrôles liés à la fusion de nom de domaine');

      /** @var ?UnityNamespace $a */
      $a=null;
      /** @var ?UnityNamespace $b */
      $b=null;
      self::buildDistinctFakeUnityNamespaces($a, $b);
      $lft = new UnityClass('New2Class');
      $lft->setNamespace($b);
      $b->addClass($lft);
      $rgt = new UnityClass('ClassA');
      UnityAssociation::generateAssociation(
          lft: $lft,
          rgt: $rgt,
          multiplicity: UnityAssociation::MANY_TO_MANY
      );

      $this
        ->given(
          description: "Je souhaite fusionné deux noms de domaines",
          a: $a,
          b: $b,
          newVersionRequired: false
        )
        ->when(
          description: "J'effectue la fusion",
          callback: function(UnityNamespace $a, UnityNamespace $b, bool &$newVersionRequired) {
            UnityNamespace::merge($a, $b, $newVersionRequired);
          }
        )
        ->then(
          description: "La fusion a bien ajouté la classe présente dans la deuxième occurence vers la première",
          callback: function(UnityNamespace $a): ?string {
            $class = $a->findClass('New2Class');
            return ($class ? $class->getName() : null);
          },
          result: 'New2Class'
        )
        ->andThen(
          description: "La fusion a bien ajouté l'association présente dans la deuxième occurence vers la première",
          callback: function(UnityNamespace $a) {
            return ($a->findAssociation('ClassANew2Class') !== null);
          },
          result: true
        )
      ;
    }

    public function testMerge(): void {
      $this->section(self::SECTION_HEADER.' Contrôles liés à la fusion d\'attributs/classes/associations');
      /** @var ?UnityNamespace $a */
      $a=null;
      /** @var ?UnityNamespace $b */
      $b=null;
      self::buildDistinctFakeUnityNamespaces($a,$b);
      /** @var UnityClass $classA */
      $classA = UnityClass::copy($a->findClass('ClassA'),$a);
      /** @var UnityAttribute $attrA */
      $attrA = UnityAttribute::copy($classA->getAttribute('propA'));
      /** @var UnityClass $classB */
      $classABis = UnityClass::copy($b->findClass('ClassA'),$b);
      /** @var UnityAttribute $attrB */
      $attrB = UnityAttribute::copy($classABis->getAttribute('propA'));

      $this
        ->given(
          description: 'Je souhaite fusionné deux attributs cohérent (même nom)',
          attrA: $attrA,
          attrB: $attrB
        )
        ->when(
          description: "J'applique la fusion sur le deuxième paramètre",
          callback: function(UnityAttribute $attrA, UnityAttribute $attrB) {
            UnityAttribute::merge($attrB, $attrA);
          }
        )
        ->then(
          description: "La fusion a mis à jour les options du deuxième paramètre",
          callback: function(UnityAttribute $attrB): ?int {
            return $attrB->getOption('theta');
          },
          result: $attrA->getOption('theta')
        )
      ;

      $this
        ->given(
          description: "Je souhaite fusionné deux classes cohérentes (même nom)",
          classA: $classA,
          classABis: $classABis
        )
        ->when(
          description: "Je fusionne les deux classes",
          callback: function(UnityClass $classA, UnityClass $classABis, ?bool &$newVersionRequired=null) {
            $newVersionRequired = false;
            UnityClass::merge($classA, $classABis, $newVersionRequired);
          }
        )
        ->then(
          description: "Les attributs de la première instance sont mis à jour",
          callback: function(UnityClass $classA) {
            $h=$classA->getAttribute('propA')->getOption('theta');
            return $h;
          },
          result: 15
        )
        ->andThen(
          description: "La fusion doit avoir signalé une nouvelle version de la classe",
          callback: function(bool $newVersionRequired) {
            return $newVersionRequired;
          },
          result: true
        )
        ->andThen(
          description: "La priorité des typages doit être valide (text devant varchar)",
          callback: function(UnityClass $classA, UnityClass $classABis) {
            return $classA->getAttribute('propA')->getType();
          },
          result: 'text'
        )
        ->andThen(
          description: "Je dois retrouver les attributs présents dans la seconde instance",
          callback: function(UnityClass $classA) {
            return (null !== $classA->getAttribute('propC'));
          },
          result: true
        )
      ;

      $this
        ->given(
          description: "Contrôle de la fusion sur les associations",
          a: $a,
          b: $b
        )
        ->when(
          description: "Je fusionne l'association de gauche de ClassBClassA en 1..1 avec l'équivalent en 0..*",
          callback: function(UnityNamespace $a, UnityNamespace $b, ?UnityAssociation &$associationA=null) {
            $classA = $a->findClass('ClassA');
            $classABis = $b->findClass('ClassA');

            $tmp = $classA->getAssociationEnds();
            $associationA = $tmp[0]->getAssociation();

            $tmp = $classABis->getAssociationEnds();
            $associationABis = null;
            foreach($tmp as $candidateAssociation) {
              $tmpAssoc = $candidateAssociation->getAssociation();
              if($tmpAssoc->getName() === $associationA->getName())
                $associationABis = $tmpAssoc;

            }

            $newVersionRequired=false;
            UnityAssociation::merge($associationA, $associationABis, $newVersionRequired);
          }
        )
        ->then(
          description: "L'association de gauche de ClassBClassA doit être en 0..*",
          callback: function(UnityAssociation $associationA):string {
            return $associationA->getAssociationLft()->getMultiplicity();
          },
          result: UnityAssociationEnd::ONE_TO_MANY
        )
      ;
    }

    public function testDuplicate(): void {
      $this->section(self::SECTION_HEADER.' Contrôles liés à la duplication d\'un espace de noms');

      $this
        ->given(
          description: "Contrôle sur la duplication d'un espace de nom et de son contenu"
        )
        ->when(
          description: "Je copie un espace de nom dans sa totalité",
          callback: function(?UnityNamespace &$copyNamespace=null,?UnityNamespace &$namespace=null) {
            $namespace = new UnityNamespace('namespace');
            $lft = new UnityClass('ClassA');
            $lft->setNamespace($namespace);

            $attr = new UnityAttribute("propA", UnityAttribute::TYPE_STRING);
            $lft->addAttribute($attr);

            $rgt = new UnityClass('ClassB');

            UnityAssociation::generateAssociation(
                lft: $lft,
                rgt: $rgt,
                multiplicity: UnityAssociation::MANY_TO_MANY
            );
            /** @var UnityNamespace $copyNamespace */
            $copyNamespace = UnityNamespace::duplicate($namespace);
          }
        )
        ->then(
          description: "La copie doit être exacte",
          callback: function(UnityNamespace $copyNamespace, UnityNamespace &$namespace) {
            return UnityNamespace::equals($copyNamespace,$namespace);
          },
          result: true
        )
      ;
    }

    public function testNamespace(): void {
      $this->section(self::SECTION_HEADER.' Contrôles liés aux espaces de nom par rapport à une classe');

      $this
        ->given(
          description: "Contrôle sur la propagation de l'espace de nom au travers des classes"
        )
        ->when(
          description: "J'initialise une classe donnée avec un espace de nom puis j'effectue une association",
          callback: function(?UnityClass &$a=null, ?UnityClass &$b=null) {
            $na = new UnityNamespace('myword');
            $a = new UnityClass('ClassA');
            $a->setNamespace($na);
            $b = new UnityClass('ClassB');
            UnityAssociation::generateAssociation(
                lft: $a,
                rgt: $b,
                multiplicity: UnityAssociation::ONE_TO_ONE
            );
          }
        )
        ->then(
          description: "L'espace de nom doit avoir été propagé lors de l'association",
          callback: function(UnityClass $b) {
            $nb = $b->getNamespace();
            return $nb ? $nb->getName() : null;
          },
          result: 'Myword'
        )
        ->andWhen(
          description: "J'effectue une association puis j'initialise une classe donnée avec un espace de nom",
          callback: function(?UnityClass &$c=null, ?UnityClass &$d=null) {
            $na = new UnityNamespace('myword');
            $c = new UnityClass('ClassA');
            $d = new UnityClass('ClassB');
            UnityAssociation::generateAssociation(
                lft: $c,
                rgt: $d,
                multiplicity: UnityAssociation::ONE_TO_ONE
            );
            $c->setNamespace($na);
          }
        )
        ->then(
          description: "L'espace de nom doit avoir été propagé lors de l'affectation à la classe",
          callback: function(UnityClass $d) {
            $nb = $d->getNamespace();
            return $nb ? $nb->getName() : null;
          },
          result: 'Myword'
        )
        ->andWhen(
          description: "J'effectue une association et j'initialise deux classes avec des espaces de nom différents",
          callback: function(?bool &$check = null) {
            $na = new UnityNamespace('myword');
            $nb = new UnityNamespace('myword2');
            $a = new UnityClass('ClassA');
            $b = new UnityClass('ClassB');
            $a->setNamespace($na);
            $b->setNamespace($nb);
            $check = false;
            try {
              UnityAssociation::generateAssociation(
                  lft: $a,
                  rgt: $b,
                  multiplicity: UnityAssociation::ONE_TO_ONE
              );
            }
            catch(CollisionNamespaceException $e) {
              $check = true;
            }
          }
        )
        ->then(
          description: "Une collision doit avoir été détectée",
          callback: function(bool $check) {
            return $check;
          },
          result: true
        )
      ;
    }

    public function testCheckAssociation(): void {

      $this->section(self::SECTION_HEADER.' Contrôles liés aux descripteurs d\'associations de classe');

      /** @var string $associationRgtName */
      $associationRgtName = "childrenOfParents";
      /** @var string $associationRgtMultiplicity */
      $associationRgtMultiplicity = UnityAssociationEnd::ZERO_TO_ONE;
      /** @var string $associationName */
      $associationName = "pinf_myLittle_relation";
      $associationNameCamelCase = "PinfMyLittleRelation";
      $this
        ->given(
          description: "Contrôle de la déclaration de classe Unity",
          associationName: $associationName
        )
        ->when(
          description: "Je créé une association entre classes",
          callback: function(
            string $associationName,
            ?UnityClass &$parentClass=null,
            ?UnityClass &$childClass=null
          ) {
            $parentClass = new UnityClass("ParentOfChildren");
            $childClass = new UnityClass("ChildrenOfParent");
            UnityAssociation::generateAssociation(
                lft: $parentClass,
                rgt: $childClass,
                multiplicity: UnityAssociation::ONE_TO_MANY,
                associationName: $associationName
            );
          }
        )
        ->then(
          description: "Je retrouve bien l'association créée depuis le parent",
          callback: function(UnityClass $parentClass) {
            /** @var Collection<index, UnityAssociationEnd> */
            $associations = $parentClass->getAssociationEnds();
            $association = $associations[0];
            return $association ? $association->getName() : null;
          },
          result: $associationRgtName
        )
        ->andThen(
          description: "L'association créée depuis le parent doit être de multiplicité 0..1",
          callback: function(UnityClass $parentClass) {
            /** @var Collection<int, UnityAssociationEnd> */
            $associationEnds = $parentClass->getAssociationEnds();
            /** @var ?AssociationEnd $associationEnd */
            $associationEnd = count($associationEnds) ? $associationEnds[0] : null;
            return $associationEnd ? $associationEnd->getMultiplicity() : null;
          },
          result: $associationRgtMultiplicity
        )
        ->andThen(
          description: "L'association porte le nom donné en paramètre sous format camel case",
          callback: function(UnityClass $parentClass, string $associationName) {
            /** @var Collection<int, UnityAssociationEnd> */
            $associationEnds = $parentClass->getAssociationEnds();
            $associationEnd = $associationEnds[0];
            $association = $associationEnd->getAssociation();
            return $association ? $association->getName() : null;
          },
          result: $associationNameCamelCase
        )
        ->andThen(
          description: "La multiplicité du l'association à droite doit être 0..*",
          callback: function(
            UnityClass $parentClass
          ) {
            /** @var Collection<int, UnityAssociationEnd> $associationEnds */
            $associationEnds = $parentClass->getAssociationEnds();
            /** @var ?UnityAssociationEnd $associationEnd */
            $associationEnd = count($associationEnds) ? $associationEnds[0] : null;
            /** @var UnityAssociation $association */
            $association = $associationEnd ? $associationEnd->getAssociation() : null;
            /** @var UnityAssociationEnd $associationEndRgt */
            $associationEndRgt = $association->getAssociationRgt();
            return $associationEndRgt ? $associationEndRgt->getMultiplicity() : null;
          },
          result: UnityAssociationEnd::ZERO_TO_MANY
        )
        ->andThen(
          description: "A partir de l'association je retrouve l'enfant associé",
          callback: function(
            UnityClass $parentClass,
            UnityClass $childClass
          ) {
            /** @var Collection<int, UnityAssociationEnd> $associationEnds */
            $associationEnds = $parentClass->getAssociationEnds();
            /** @var ?UnityAssociationEnd $associationEnd */
            $associationEnd = count($associationEnds) ? $associationEnds[0] : null;
            /** @var UnityAssociation $association */
            $association = $associationEnd ? $associationEnd->getAssociation() : null;
            /** @var UnityAssociationEnd $associationEndRgt */
            $associationEndRgt = $association->getAssociationRgt();
            /** @var UnityClass $targetClass */
            $targetClass = $associationEndRgt->getClass();
            return ($childClass == $targetClass);
          },
          result: true
        )
      ;
      $this
        ->given(
          description: "Recherche d'une classe dans Unity"
        )
        ->when(
          description: "Je déclare un ensemble de classe associées",
          callback: function(?UnityClass &$a=null) {
            $a = new UnityClass("ClassA");
            $b = new UnityClass("ClassB");
            $c = new UnityClass("ClassC");
            $d = new UnityClass("ClassD");
            UnityAssociation::generateAssociation(
                lft: $a,
                rgt: $b,
                multiplicity: UnityAssociation::ONE_TO_MANY
            );
            UnityAssociation::generateAssociation(
                lft: $d,
                rgt: $c,
                multiplicity: UnityAssociation::ONE_TO_MANY
            );
            UnityAssociation::generateAssociation(
                lft: $c,
                rgt: $b,
                multiplicity: UnityAssociation::MANY_TO_MANY
            );
          }
        )
        ->then(
          description: "Je peux retrouver une classe par les liaisons",
          callback: function(UnityClass $a) {
            $d = $a
              ->findClass('ClassD')
            ;
            return $d ? $d->getName() : null;
          },
          result: 'ClassD'
        )
        ->andThen(
          description: "Je peux retrouver l'ensemble des classes du modèle",
          callback: function(UnityClass $a) {
            $classes = array_keys($a->findClasses());
            return
              in_array('ClassA', $classes) &&
              in_array('ClassB', $classes) &&
              in_array('ClassC', $classes) &&
              in_array('ClassD', $classes)
            ;
          },
          result: true
        )
      ;
    }
}
