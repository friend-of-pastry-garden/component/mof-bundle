Description du module MOFBundle
==

Introduction
--

Le module MOFBundle provient d'un concept d'informatique théorique : le MOF (Meta Object Facility). Le périmètre du module a été restreint à la partie donnée, le segment OCL n'étant pas assez valorisable sur les cas d'usage identifiés. L'objectif est d'opérer des manipulation sur des modèles (fichier XML, base SQL, ...) afin d'effectuer des opérations de transformations à destination de tiers.

Installation
--

1. Déclaration du package au sein du composer.json

Rajouter le repository externe pour le bundle mof de la manière suivante :

```yaml
"repositories": [
  ...
  {
    "type": "vcs",
    "url": "https://gitlab.adullact.net/friend-of-pastry-garden/component/mof-bundle.git"
  }
  ...
]
```

2. Appel du bundle pour votre application

``
composer require friend-of-pastry-garden/mof-bundle
``

3. Configuration du proxy

Le proxy se configure directement au niveau de l'environnement de la machine. Une fois fait il sera pris en compte automatiquement par le robot.

#### Exemple de déclaration de proxy depuis le Dockerfile

```bash
ENV http_proxy "http://rie-proxy.justice.gouv.fr:8080"
ENV https_proxy "http://rie-proxy.justice.gouv.fr:8080"
ENV ftp_proxy "http://rie-proxy.justice.gouv.fr:8080"
ENV no_proxy ".intranet.justice.gouv.fr,localhost,127.0.0.1"
```

4. Configuration de l'environnement de test

L'environnement de test est fonction de l'écosystème de l'équipe qui doit intégrer l'applicatif. Un ensemble de variable d'environnement doit être définie dans le .env.test.local

| variable | utilisation | exemple |
| -- | -- | -- |
| TEST__IGNORE_WIP | Variable de travail - utilisé pour désactiver les tests en cours de construction | true |

Glossaire
--
Dans la suite du document, nous allons utiliser régulièrement une terminologie qui nécessite d'être précisée dès à présent.

* Modèle : représentation décrivant un objet numérique (ex: un XML décrivant une nature d'infraction)
* Nom de domaine : nom usuel d'une représentation servant à l'identifier de manière unique. Un nom de domaine peut avoir plusieurs représentations qui seront distinguées entre elles par un numéro de version.
* Classe : Représentation d'un concept (ex: une nature d'infraction)
* Attribut : descripteur d'une information (ex: Pour une nature d'infraction son numéro, son libellé ...)
* Association : Nature du liant entre classes (ex: une nature d'infraction peut avoir entre 0 et N versions).
* Méta-modèle : représentation décrivant les règles de construction des relations entre objets, attributs et associations (ex: une nature d'infraction contient un numéro et est associé à plusieurs versions)

Première étape : l'acquisition d'une source de donnée
--

L'objectif du module mof est de convertir une source de donnée vers un autre type de source de donnée. Pour ce faire, le module utilise un modèle pivot de transformation, Unity, qui fait l'intermédiaire.

*Représentation du modèle Unity*

![](docs/unity.png)

A titre d'illustration, supposons un référentiel de données exporté au format XML que nous souhaitons moderniser. Pour mener à bien la transition, nous allons devoir opérer les actions suivantes au travers d'Unity :

1. Extraction du méta-modèle de donnée à partir de la source de donnée.

Dans l'exemple, nous pourrons utiliser la DTD associée au XML, ou à défaut le XML lui-même.

2. Consolidation du méta-modèle par apprentissage au travers d'un corpus documentaire

Nous consolidons les règles concernant la source de donnée. Dans l'exemple, nous pourrons exploiter le XML afin de typer les attributs, trouver la longueur maximale pour une colonne donnée, identifier des énumérations ...

3. Déclaration des critères d'unicité des classes

Pour construire des instances de classe, nous devons auparavant définir ce qui en font des éléments uniques.
 Nous allons différencier les informations immutables (son identifiant, son code ...) des informantions variables (date de dernière mise à jour ...). Par défaut, les attributs d'une instance sont tous immutables et les associations sont toutes variables.

Deuxième étape : consignation des données
--

La consignation consiste à stocker un ensemble d'instances de classe, d'attributs et d'associations.

Les comportements différent selon la nature d'instance.

### Instance d'attribut

Une instance d'attribut stocke l'information primitive (un code, un libellé ...).

#### Ajout d'une nouvelle instance d'attribut
Une instance d'attribut est la concaténation de l'heure de création et d'une valeur au format texte. Un hash lui est assigné définitivement. Une réplique de l'attribut lui est attachée avec le numéro de version 1.

#### Mise à jour d'une instance d'attribut
La mise à jour n'est possible que si l'attribut n'est pas immutable. Une nouvelle réplique est attachée à l'attribut avec la valeur de remplacement. Son numéro de version vaut "(nombre de version déjà présente)+1".

#### Suppression d'une instance d'attribut
La suppression d'un attribut n'est possible que si l'attribut n'est pas immutable. Il s'agit d'une suppression logique. On met à jour la date de suppression de la dernière version et la date de suppression de l'attribut.

### Instance de classe

#### Ajout d'une nouvelle instance de classe
Une instance de classe ne porte pas d'information en soit. Sa clé de hash n'est que la concaténation de sa date de création et des hash immutables d'attributs et/ou d'associations. En parallèle, une représentation de l'ensemble des attributs et/ou associations immutables est ajoutée sous format JSONB à l'instance. Cela servira à la recherche optimisée de l'instance dans le SGBD.

#### Mise à jour d'une nouvelle instance de classe
Les mises à jour d'attributs et/ou associations n'entraînent pas d'action à mener au niveau de la classe.

#### Suppression d'une instance de classe
La suppression est logique et consiste à mettre à jour la date de suppression. Elle n'est possible que si l'instance n'est pas en association par immutabilité. Si tel est le cas, une option de forçage permet la suppression à toutes les instances de classe en contrepartie ainsi que l'association elle-même. Cela garanti l'intégrité du modèle.

### Instance d'association

#### Ajout d'une instance d'association
La génération des hashs est déterminée selon l'immutabilité des classes en correspondances. L'immutabilité impose un mode opératoire d'exécution des tâches.

1. Le bord gauche de l'association est immutable

Les instances de classe du bord gauche doivent disposer impérativement d'un hash. A contrario les instances de classe du bord droit doivent impérativement ne pas avoir de hash.

L'association va aggréger l'ensemble des classes du bord gauche et construire la clé de hash *hashLft* a partir des hashs récolté. Cette clé va permettre aux classes du bord droit d'utiliser la clé de hahs *hashLft* à leur clé de génération de hash.

Une fois les classes générées, on génére la clé de hash du bord droit *hashRgt*.

Une réplique de l'association lui est attachée avec le numéro de version 1.

2. Le bord droit de l'association est immutable

Les instances de classe du bord droit doivent disposer impérativement d'un hash. A contrario les instances de classe du bord gauche doivent impérativement ne pas avoir de hash.

L'association va aggréger l'ensemble des classes du bord droit et construire la clé de hash *hashRgt* a partir des hashs récolté. Cette clé va permettre aux classes du bord gauche d'utiliser la clé de hahs *hashRgt* à leur clé de génération de hash.

Une fois les classes générées, on génére la clé de hash du bord gauche *hashLft*.

Une réplique de l'association lui est attachée avec le numéro de version 1.

3. Les deux bords sont immutables

Ce cas doit être vérifié comme étant impossible !

#### Mise à jour d'une instance d'association

Seul un bord non immutable peut être mis à jour. Dans ce cas une nouvelle réplique de l'association est rattachée. Son numéro de version vaut "(nombre de version déjà présente)+1".

#### Suppression d'une instance d'association

Ce cas n'intervient que si les deux bords ne sont pas immutables. La suppression n'est que logique. Elle entraîne la mise à jour de la date de suppression sur la dernière version et sur l'instance d'association.

Utilisation
--
Une documentation est réalisée par thématique :

[Etape 1 : Extraction du modèle de représentation](/docs/MOF_dump.md)
[Etape 2 : Mise à jour du modèle de représentation](/docs/MOF_merge.md)
