<?php

namespace FOPG\Component\MOFBundle\Contracts;

use FOPG\Component\MOFBundle\Contracts\Response\ResponseInterface;

interface SerializerInterface
{
  const AS_CLASS = 'class';
  const AS_ATTRIBUTE = 'attribute';

  public static function getExtensions(): array;
  public function render(): ResponseInterface;
}
