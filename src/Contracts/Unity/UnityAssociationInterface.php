<?php

namespace FOPG\Component\MOFBundle\Contracts\Unity;

interface UnityAssociationInterface
{
  const ONE_TO_ONE  = '1..1';
  const ONE_TO_MANY = '1..*';
  const MANY_TO_ONE = '*..1';
  const MANY_TO_MANY= '*..*';
}
