<?php

namespace FOPG\Component\MOFBundle\Contracts\Unity;

interface UnityClassInterface {
  const ROOT_CLASS = 'root';
}
