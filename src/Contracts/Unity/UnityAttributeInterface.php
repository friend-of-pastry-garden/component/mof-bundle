<?php

namespace FOPG\Component\MOFBundle\Contracts\Unity;

interface UnityAttributeInterface
{
  const TYPE_STRING = "varchar";
  const TYPE_DATE = "date";
  const TYPE_INTEGER = "integer";
  const TYPE_TEXT = "text";
  const TYPE_ENUM = 'enum';

  const MAXLENGTH_STRING = 255;
  const MAXSIZE_ENUM = 10;

  const OPTION_STRING_MAXLENGTH = 'maxlength';
}
