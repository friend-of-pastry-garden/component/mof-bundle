<?php

namespace FOPG\Component\MOFBundle\Serializer;

use FOPG\Component\MOFBundle\Contracts\Response\ResponseInterface;
use FOPG\Component\MOFBundle\Contracts\Unity\UnityAttributeInterface;
use FOPG\Component\MOFBundle\Contracts\Unity\UnityAssociationEndInterface;
use FOPG\Component\MOFBundle\Contracts\Unity\UnityClassInterface;
use FOPG\Component\MOFBundle\Exception\Dtd\UnexpectedOperatorException;
use FOPG\Component\MOFBundle\Exception\Dtd\InvalidOperatorException;
use FOPG\Component\MOFBundle\Exception\Dtd\InvalidTypeException;
use FOPG\Component\MOFBundle\Serializer\Dtd\DtdClass;
use FOPG\Component\MOFBundle\Serializer\Response\JsonResponse;
use FOPG\Component\MOFBundle\Serializer\Unity\UnityAssociation;
use FOPG\Component\MOFBundle\Serializer\Unity\UnityAttribute;
use FOPG\Component\MOFBundle\Serializer\Unity\UnityClass;

class DtdSerializer extends AbstractSerializer
{
  public static function getExtensions(): array {
    return ["dtd"];
  }

  public function __construct(?string $filename=null, ?string $namespace=null) {
    parent::__construct($filename,$namespace);
    $this->setRoot();
  }

  /**
   * Elimination des éléments rattachés par Root à l'initialisation de manière facultative.
   * Ces éléments sont ceux qui ont au moins une association à droite autre que Root
   *
   * @return self
   */
  public function cleanupRoot(): self {
    /** @var UnityClass $root */
    $root = $this->getRoot();
    /** @var array<string, int> $classes */
    $classes = $root->findClasses();
    /** @var string $class */
    foreach(array_keys($classes) as $class) {
      $current = $root->findClass($class);
      /** @var array<int, UnityAssociationEnd> $assocEnds */
      $assocEnds = $current->getAssociationLftEnds();
      /** @var int $cpt */
      $cpt = count($assocEnds);
      if($cpt > 1) {
        /** @var UnityAssociationEnd $assocEnd */
        foreach($assocEnds as $assocEnd) {
          /** @var UnityAssociation $assoc */
          $assoc = $assocEnd->getAssociation();
          $childClass = $assoc
            ->getAssociationLft()
            ->getClass()
          ;
          if(UnityClassInterface::ROOT_CLASS === $childClass->getOriginName())
            UnityAssociation::removeAssociation($assoc);
        }
      }
    }
    return $this;
  }

  public function setRoot(): self {
    /** @var ?string $filename */
    $filename = $this->getFilename();
    if(file_exists($filename)) {
      /** @var string $content */
      $content = file_get_contents($filename);
      /** @var array<int, string> $lines */
      $lines = self::findElements($content);
      /** @var array<int, string> $pcDataElements */
      $pcDataElements = self::findPCDataElements(lines: $lines);
      $this->buildClasses(lines: $lines,pcDataElements: $pcDataElements);
      $this->cleanupRoot();
    }

    return $this;
  }

  /**
   * Normalisation des sous-classes ou attributs trouvés à partir d'une ligne ELEMENT
   *
   * @param string $attributeOrClasses
   * @return renvoi des sous-classes ou attributs
   */
  private static function findAttributesOrClasses(string $attributeOrClasses, array $pcDataElements): array {
    $output = [];

    # (A|B)? => A?, B?
    preg_match_all("/[(](?<content>[^)]+)[)](?<operator>[+?]?)/", $attributeOrClasses, $matches);
    $contents = $matches['content'];
    $operators = $matches['operator'];
    array_walk($contents, function(string $content, int $index)use(&$attributeOrClasses, $operators) {
      $operator = $operators[$index];
      $target = "($content)".$operator;
      if(preg_match("/[|]/", $content) && "" === $operator)
        $operator="?";
      $replacement = implode($operator.",",explode("|", $content)).$operator;
      $attributeOrClasses = str_replace($target, $replacement, $attributeOrClasses);
    });

    # détection des classes ou attributs à partir de la chaîne propre
    preg_match_all("/(?<classOrAttribute>[^?+,]+)(?<operator>[?+]?)/i", $attributeOrClasses, $matches);
    $classOrAttributes = $matches['classOrAttribute'];
    $operators = $matches['operator'];
    array_walk($classOrAttributes, function(string $classOrAttribute, int $index) use($operators, &$output, $pcDataElements) {
      $operator = $operators[$index];
      /** @var bool $isAttribute */
      $isAttribute = in_array($classOrAttribute, $pcDataElements);
      $output[]=[
        'type'    => (true === $isAttribute) ? self::AS_ATTRIBUTE : self::AS_CLASS,
        'operator'=> self::castDTDToUnityOperator($operator),
        'name'    => $classOrAttribute
      ];
    });
    return $output;
  }

  public static function castDTDToUnityOperator(string $operator): string {
    switch($operator) {
      case "":
        return UnityAssociationEndInterface::ONE_TO_ONE;
        break;
      case "?":
        return UnityAssociationEndInterface::ZERO_TO_ONE;
        break;
      case "+":
        return UnityAssociationEndInterface::ZERO_TO_MANY;
        break;
      default:
        throw new InvalidOperatorException($operator);
    }
  }

  /**
   * Alimentation des classes Unity de l'objet courant
   *
   * @param array $lines Lignes DTD contenant les informations de classe
   * @param array $pcDataElements Liste des paramètres étant de type PCDATA
   * @return void
   */
  private function buildClasses(array $lines, array $pcDataElements): void {
    /** @var array $subClasseOrAttributes */
    $subClasseOrAttributes=[];
    /**
     * @var int $index
     * @var string $line
     */
    foreach($lines as $index => $line) {
      if(preg_match("/[<][!]ELEMENT[ ]+(?<class>\w+)[ ]*[(](?<subclasses>.*)[)]/i", $line, $matches)) {
        /** @var string $class */
        $class = $matches['class'];
        /** @var string $subStrClasses */
        $subStrClasses = str_replace(" ", "",$matches['subclasses']);
        $subClasseOrAttributes = array_merge(
          $subClasseOrAttributes,
          [
            $class => self::findAttributesOrClasses(
              attributeOrClasses: $subStrClasses,
              pcDataElements: $pcDataElements
            )
          ]
        );
      }
    }
    $this->populateUnity($subClasseOrAttributes);
  }

  /**
   * Alimentation du modèle Unity à partir de la DTD
   *
   * @param array $model Modèle descriptif DTD
   * @return void
   */
  public function populateUnity(array $models): void {

    /** @var UnityClass $root */
    $root = $this->getRoot();
    /**
     * @var string $classname
     * @var array $attributeOrClasses
     */
    foreach($models as $classname => $attributeOrClasses) {
      /** @var ?UnityClass $uClass */
      $uClass = $root->findClass($classname);
      if(null === $uClass)
        $uClass = new UnityClass($classname);

      UnityAssociation::generateAssociation(
          lft: $root,
          rgt: $uClass,
          multiplicity: UnityAssociation::ONE_TO_ONE
      );

      foreach($attributeOrClasses as $attributeOrClasse) {
        /** @var ?string $type */
        $type = $attributeOrClasse['type'] ?? null;
        /** @var ?string $operator */
        $operator = $attributeOrClasse['operator'] ?? null;
        /** @var ?string $name */
        $name = $attributeOrClasse['name'] ?? null;
        switch($type) {
          case self::AS_ATTRIBUTE:
            /** @var UnityAttribute $tmp */
            $tmp = new UnityAttribute($name, UnityAttribute::TYPE_STRING);
            switch($operator) {
              case UnityAssociationEndInterface::ZERO_TO_ONE:
                $tmp->setNullable(true);
                break;
              case UnityAssociationEndInterface::ONE_TO_ONE:
                break;
              default:
                throw new UnexpectedOperatorException($operator);
            }
            $uClass->addAttribute($tmp);
            break;
          case self::AS_CLASS:
            /** @var ?UnityClass $tmp */
            $tmp = $root->findClass($name);
            if(null === $tmp)
              $tmp = new UnityClass($name);
            /** @var ?string $globalOperator */
            $globalOperator = null;
            switch($operator) {
              case UnityAssociationEndInterface::ZERO_TO_ONE:
              case UnityAssociationEndInterface::ONE_TO_ONE:
                $globalOperator = $operator;
                break;
              case UnityAssociationEndInterface::ZERO_TO_MANY:
              case UnityAssociationEndInterface::ONE_TO_MANY:
                $globalOperator = UnityAssociation::ONE_TO_MANY;
                break;
              default:
                throw new UnexpectedOperatorException($operator);
            }

            UnityAssociation::generateAssociation(
              lft: $uClass,
              rgt: $tmp,
              multiplicity: $globalOperator
            );

            break;
          default:
            throw new InvalidTypeException($type);
        }
      }
    }
  }

  /**
   * Extraction des éléments de type PCDATA.
   *
   * L'ensemble des éléments de ce type sont exclues du tableau source
   *
   * @param array $lines
   * @return array
   */
  private static function findPCDataElements(array &$lines): array {
    /** @var array $output */
    $output=[];
    /**
     * @var int $index
     * @var string $line
     */
    foreach($lines as $index => $line) {
      if(preg_match("/^[<][!]ELEMENT[ ]+(?<attribute>[^(]+)[ ]*[(][#]PCDATA[)][ ]*[>]$/i", trim($line), $matches)) {
        unset($lines[$index]);
        $output[]=trim($matches['attribute']);
      }
    }
    return $output;
  }
  /**
   * Détection des éléments composants la DTD
   *
   * @param string $content
   * @return array
   */
  private static function findElements(string $content): array {
    /** @var array $lines */
    $lines = explode("\n", $content);
    $output = [];
    $opened = false;
    $index = 0;
    foreach($lines as $line)
    {
      $line = preg_replace("/[ ]+/", " ",trim(str_replace(["\t","\r"],"", $line)));
      if(strlen($line))
      {
        // on détecte un nouvel élément
        if(preg_match("/^[<][!]ELEMENT/i", $line))
        {
          $output[$index]=$line;
          $opened = (bool)!preg_match("/[>]$/",$line);
        }
        // on se situe sur une ligne après l'ouverture de l'élément
        elseif(true === $opened)
          $output[$index].=$line;
        // on est en fin de déclaration d'élément
        if(preg_match("/[>]$/", $line))
          $index++;
      }
    }
    return $output;
  }

  public function render(): ResponseInterface {

    /**
     * @todo
     */
  return new JsonResponse(['todo']);

  }
}
