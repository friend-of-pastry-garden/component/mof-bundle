<?php

namespace FOPG\Component\MOFBundle\Serializer;

use FOPG\Component\MOFBundle\Contracts\Response\ResponseInterface;
use FOPG\Component\MOFBundle\Serializer\Response\JsonResponse;
use FOPG\Component\MOFBundle\Serializer\Unity\UnityClass;
use FOPG\Component\MOFBundle\Serializer\XmlConverter\XmlToDtdConverter;
use FOPG\Component\MOFBundle\Serializer\XmlConverter\XmlToPhpConverter;
use FOPG\Component\UtilsBundle\SimpleHtml\SimpleHtml;

class XmlSerializer extends AbstractSerializer
{
  /**
   * Définition du minimum de critères significatifs pour une analyse des types
   * à partir d'un document
   */
  const MINIMUM_ITEMS_FOR_SIGNIFIANT=10;

  use XmlToDtdConverter;
  use XmlToPhpConverter;

  private ?\SimpleXMLElement $_xmlInstance=null;

  public static function getExtensions(): array {
    return ["xml"];
  }

  public function __construct(?string $filename=null, ?string $namespace=null) {
    parent::__construct($filename, $namespace);
    /** @var \SimpleXMLElement $xml */
   $this->_xmlInstance = file_exists($filename) ? \simplexml_load_file($filename) : null;
  }

  public function getXmlInstance(): ?\SimpleXMLElement {
    return $this->_xmlInstance;
  }

  /**
   * Récupération des instances d'une classe Unity
   *
   * @param UnityClass $class
   * @return  array<int, \SimpleXMLElement>
   */
  public function analyzeAttributeTypes(UnityClass $class): array {
    $root = $this->getXmlInstance();
    if(null === $root)
      return null;
    // @var string $xpath
    $xpath = "//".$class->getOriginName();
    // @var array $datas
    $datas = $root->xpath($xpath);
    $informations = self::get_stats_from_xml($datas, $class);
    return $informations;
  }

  public function render(): ResponseInterface {
    /** @var ?string $file */
    $file = $this->getFilename();
    /** @var ?string $content */
    $content = file_get_contents($file);
    if(!empty($content)) {
      /** @var SimpleHtmlDom $dom */
      $dom = SimpleHtml::str_get_xml($content)->getContainer();
      /** @var array $tab */
      $tab = $dom->toArray();
      return new JsonResponse($tab);
    }
  }

  public static function guess_types(XmlSerializer $xml, DtdSerializer $dtd): void {
    /** @var UnityClass $root */
    $root = $dtd->getRoot();
    /** @var array $classNames Nom des classes à traiter triés par ordre de traitement */
    $classNames = $root->findClasses();
    /** @var string $className */
    foreach(array_keys($classNames) as $className) {
      /** @var UnityClass $classNode */
      $classNode = $root->findClass($className);
      /** @var array<int, \SimpleXMLElement> $output */
      $output = $xml->analyzeAttributeTypes($classNode);
    }
  }
}
