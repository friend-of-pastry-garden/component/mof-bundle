<?php

namespace FOPG\Component\MOFBundle\Serializer\Unity;

use FOPG\Component\MOFBundle\Contracts\Unity\UnityAssociationEndInterface;
use FOPG\Component\MOFBundle\Exception\Unity\InconsistencyAssociationEndException;

class UnityAssociationEnd extends AbstractUnityCommon implements UnityAssociationEndInterface
{
  private ?UnityAssociation $_association = null;
  private ?unityClass $_class = null;
  private ?string $_multiplicity = null;
  private bool $_partOfHash = false;

  public function __construct(string $name) {
    parent::__construct($name);
  }

  /**
   * Récupération de la terminaison d'association inverse
   *
   * @return ?UnityAssociationEnd
   */
  public function getReverseAssociationEnd(): ?UnityAssociationEnd {
    /** @var ?UnityAssociation $association */
    $association = $this->getAssociation();
    /** @var ?UnityAssociationEnd $associationLft */
    $associationLft = $association ? $association->getAssociationLft() : null;
    /** @var ?UnityAssociationEnd $associationRgt */
    $associationRgt = $association ? $association->getAssociationRgt() : null;
    if($associationLft === $this)
      return $associationRgt;
    return $associationLft ?? $this;
  }

  /**
   * @return string
   */
  public function getName(): string {
    /** @var ?AssociationEnd $reverseAssocEnd */
    $reverseAssocEnd = $this->getReverseAssociationEnd();
    /** @var string $assocEndName */
    $assocEndName = parent::getName();
    if(null === $reverseAssocEnd)
      return $assocEndName;

    switch($reverseAssocEnd->getMultiplicity()) {
      case self::ONE_TO_ONE:
      case self::ZERO_TO_ONE:
        break;
      case self::ZERO_TO_MANY:
      case self::ONE_TO_MANY:
        $assocEndName.=(preg_match("/s$/",$assocEndName) ? 'es' : 's');
        break;
      default:
    }
    return $assocEndName;
  }

  public static function merge(UnityAssociationEnd $a, UnityAssociationEnd $b, bool &$newVersionRequired=false): void {
    if($a->getName() !== $b->getName())
      throw new InconsistencyAssociationEndException($a,$b);
    self::merge_multiplicity($a,$b);
    $newVersionRequired=false;
    self::merge_part_of_hash($a, $b, $newVersionRequired);
  }

  /**
   * Sous fonction de mise à jour de participation au hash
   *
   * @todo implémenter cette fonctionnalité (pour l'instant inutile)
   * @param UnityAssociationEnd $a
   * @param UnityAssociationEnd $b
   * @param bool $newVersionRequired
   * @return void
   */
  private static function merge_part_of_hash(UnityAssociationEnd $a, UnityAssociationEnd $b, bool &$newVersionRequired=false): void {

  }

  /**
   * Conversion de la multiplicité d'une terminaison d'association en entier
   *
   * @param UnityAssociationEnd $a
   * @return int
   */
  private static function get_multiplicity_int(UnityAssociationEnd $a): int {
    switch($a->getMultiplicity()) {
      case self::ZERO_TO_ONE:
        return 1;
      case self::ONE_TO_ONE:
        return 2;
      case self::ZERO_TO_MANY:
        return 3;
      case self::ONE_TO_MANY:
        return 4;
      return 0;
    }
  }

  /**
   * Sous fonction de gestion de la multiplicité
   *
   * @param UnityAssociationEnd $a
   * @param UnityAssociationEnd $b
   * @return void
   */
  private static function merge_multiplicity(UnityAssociationEnd $a, UnityAssociationEnd $b): void {
    /** @var int $scoreA */
    $scoreA = self::get_multiplicity_int($a);
    /** @var int $scoreB */
    $scoreB = self::get_multiplicity_int($b);
    if($scoreA < $scoreB)
      $a->setMultiplicity($b->getMultiplicity());
  }

  public static function copy(UnityAssociationEnd $associationEnd, UnityClass $copyClass): UnityAssociationEnd {
    $copy = new UnityAssociationEnd($associationEnd->getOriginName());
    $copy->setMultiplicity($associationEnd->getMultiplicity());
    $copy->setPartOfHash($associationEnd->isPartOfHash());
    $copy->setClass($copyClass);
    return $copy;
  }

  public static function equals(UnityAssociationEnd $a, UnityAssociationEnd $b): bool {
    $check = true;
    $check = $check && ($a->getName() === $b->getName());
    $check = $check && ($a->getMultiplicity() === $b->getMultiplicity());
    $check = $check && ($a->isPartOfHash() === $b->isPartOfHash());
    $check = $check && UnityClass::equals($a->getClass(), $b->getClass());
    return $check;
  }

  public function setPartOfHash(bool $partOfHash): self {
    $this->_partOfHash = $partOfHash;
    return $this;
  }

  public function isPartOfHash(): bool {
    return $this->_partOfHash;
  }

  public function setAssociation(UnityAssociation $association): self {
    $this->_association = $association;
    return $this;
  }

  public function getAssociation(): ?UnityAssociation {
    return $this->_association;
  }

  public function setClass(UnityClass $class): self {
    $this->_class = $class;
    return $this;
  }

  public function getClass(): ?UnityClass {
    return $this->_class;
  }

  public function setMultiplicity(string $multiplicity): self {
    $this->_multiplicity = $multiplicity;
    return $this;
  }

  public function getMultiplicity(): ?string {
    return $this->_multiplicity;
  }
}
