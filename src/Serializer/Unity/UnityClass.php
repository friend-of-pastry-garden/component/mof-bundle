<?php

namespace FOPG\Component\MOFBundle\Serializer\Unity;

use FOPG\Component\MOFBundle\Contracts\Unity\UnityAssociationEndInterface;
use FOPG\Component\MOFBundle\Contracts\Unity\UnityClassInterface;
use FOPG\Component\MOFBundle\Exception\Unity\InconsistencyClassException;
use FOPG\Component\UtilsBundle\Collection\Collection;
use FOPG\Component\UtilsBundle\String\StringFacility;

class UnityClass extends AbstractUnityCommon implements UnityClassInterface
{
  /** @var Array<int, UnityAttribute> */
  private array $_attributes = [];
  /** @var Array<int, UnityAssociationEnd> */
  private array $_associations = [];
  /** @var ?UnityNamespace */
  private ?UnityNamespace $_namespace = null;

  public function __construct(string $name) {
    parent::__construct($name);
  }

  /**
   * Fonction de copie limitée d'une classe
   *
   * @param UnityClass $class
   * @return UnityClass
   */
  public static function copy(UnityClass $class, ?UnityNamespace $copyNamespace): UnityClass {
    /** @var UnityClass $copy */
    $copy = new UnityClass($class->getName());
    if(null !== $copyNamespace)
      $copy->setNamespace($copyNamespace);
    /** @var UnityAttribute $attribute */
    foreach($class->getAttributes() as $attribute) {
      $copyAttr = UnityAttribute::copy($attribute);
      $copy->addAttribute($copyAttr);
    }
    return $copy;
  }

  /**
   * Fusion des informations entre classe cohérente
   *
   * Le résultat est stocké sur la première variable $a
   *
   * @param UnityClass $a
   * @param UnityClass $b
   * @param bool $newVersionRequired
   * @return void
   */
  public static function merge(UnityClass $a, UnityClass $b, bool &$newVersionRequired=false): void {
    if($a->getName() !== $b->getName())
      throw new InconsistencyClassException($a,$b);
    foreach($a->getAttributes() as $attrA) {
      $attrB = $b->getAttribute($attrA->getName());
      if(null !== $attrB) {
        $tmp = false;
        UnityAttribute::merge($attrA, $attrB, $tmp);
        $newVersionRequired = $newVersionRequired || $tmp;
      }
    }

    foreach($b->getAttributes() as $attrB) {
      $attrA = $a->getAttribute($attrB->getName());
      if(null === $attrA) {
        $a->addAttribute(UnityAttribute::copy($attrB));
        $newVersionRequired=true;
      }
    }
  }

  /**
   * @param UnityClass $a
   * @param UnityClass $b
   * @return bool
   */
  public static function equals(UnityClass $a, UnityClass $b): bool {
    $check = true;
    $check = $check && ($a->getOriginName() === $b->getOriginName());
    $check = $check && ($a->getName() === $b->getName());

    foreach($a->getAttributes() as $attributeA) {
      $tmp = $b->getAttribute($attributeA->getName());
      $check = $check && UnityAttribute::equals($tmp, $attributeA);
    }

    foreach($b->getAttributes() as $attributeB) {
      $tmp = $a->getAttribute($attributeB->getName());
      $check = $check && UnityAttribute::equals($tmp, $attributeB);
    }

    return $check;
  }

  public function setNamespace(UnityNamespace $namespace): self {
    $this->_namespace = $namespace;
    $namespace->addClass($this);

    foreach(array_keys($this->findClasses()) as $classname) {
      $class = $this->findClass($classname);
      UnityNamespace::merge_namespace($this, $class);
    }
    return $this;
  }

  public function getNamespace(): ?UnityNamespace {
    return $this->_namespace;
  }

  /**
   * Récupération des constituants de Hash. Ils marquent les paramètres
   * d'unicité des instances de la classe
   *
   * @return array Talbeau des noms de paramètres
   */
  public function getPartsOfHash(): array {
    $output=[];
    foreach($this->getAttributes() as $attribute)
      if(true === $attribute->isPartOfHash())
        $output[]=$attribute->getName();
    foreach($this->getAssociationEnds() as $ae)
      if(true === $ae->isPartOfHash())
        $output[]=$ae->getName();
    return $output;
  }

  /**
   * Recherche d'une déclaration de classe en parcourant le réseau d'association
   *
   * @param string $name
   * @param int $depth
   * @return ?UnityClass Renvoi de la déclaration de classe
   */
  public function findClass(
    string $name,
    array &$classesAllreadyFound=[]
  ): ?UnityClass {
    $normalizedName = self::normalizeClassname($name);
    return self::search_class($this, $normalizedName);
  }
  /**
   * Récupération des classes par ordre de hiérarchie
   *
   * @return array<string,int>
   */
  public function findClasses(): array {
    $output = [];
    $name = '__no_name_'.rand(1000,9999);
    $normalizedName = self::normalizeClassname($name);
    self::search_class($this, $normalizedName, $output);
    $min=0;
    array_walk($output, function($priority, $className)use(&$min) {
      $min=($min > $priority) ? $priority : $min;
    });
    foreach($output as $className => $priority)
      $output[$className]-=$min;
    /** @todo trier via countingSort */
    $collection = new Collection(
      $output,
      function(string $index, int $priority): int { return $priority; },
      /** Fonction de comparaison pour le tri */
      function(int $keyA, int $keyB): bool { return ($keyA<$keyB); },
      function(string $index, int $priority): string { return $index; }
    );
    $collection->countingSort();

    $newOutput = [];
    for($i=0;$i< $collection->count();$i++)
      $newOutput[$collection->get_value_by_index($i)]=$collection->get_key_by_index($i);

    return $newOutput;
  }

  /**
   * Récupération de l'ensemble des associations à l'intérieur du réseau de classe
   *
   * @return array<int,string>
   */
  public function findAssociations(): array {
    $associations=[];
    foreach(array_keys($this->findClasses()) as $classname) {
      foreach($this->findClass($classname)->getAssociationEnds() as $associationEnd) {
        $associations[]=$associationEnd->getAssociation()->getName();
      }
    }
    return array_unique($associations);
  }

  /**
   * Récupération d'une association à l'intérieur du réseau de classe
   *
   * @param string $name
   * @return ?UnityAssociation
   */
  public function findAssociation(string $name): ?UnityAssociation {
    foreach(array_keys($this->findClasses()) as $classname) {
      foreach($this->findClass($classname)->getAssociationEnds() as $associationEnd) {
        $association = $associationEnd->getAssociation();
        if($association->getName() === $name)
          return $association;
      }
    }
    return null;
  }

  public static function normalizeClassname(string $classname): string {
    return ucfirst(
      StringFacility::toCamelCase(
        StringFacility::toSnakeCase($classname)
      )
    );
  }

  private static function search_class(
    UnityClass $referent,
    string $classname,
    array &$classesAllreadyFound=[],
    int $depth=0
  ): ?UnityClass {
    /** @var string $currentClassname */
    $currentClassname = $referent->getName();
    if($currentClassname === $classname)
      return $referent;
    $classesAllreadyFound[$currentClassname]=$depth;

    /** @var UnityAssociationEnd $associationEnd */
    foreach($referent->getAssociationEnds() as $associationEnd) {
      $association = $associationEnd->getAssociation();

      $associationEndLeft = $association ? $association->getAssociationLft() : null;
      $classLeft = $associationEndLeft ? $associationEndLeft->getClass() : null;
      $classnameLeft = $classLeft->getName();
      if(!in_array($classnameLeft, array_keys($classesAllreadyFound))) {
        /** @var ?UnityClass $obj */
        $obj = self::search_class(
          referent: $classLeft,
          classname: $classname,
          classesAllreadyFound: $classesAllreadyFound,
          depth: $depth-1
        );
        if(null !== $obj)
          return $obj;
      }

      $associationEndRight = $association ? $association->getAssociationRgt() : null;
      $classRight = $associationEndRight ? $associationEndRight->getClass() : null;
      $classnameRight = $classRight->getName();
      if(!in_array($classnameRight, array_keys($classesAllreadyFound))) {
        /** @var ?UnityClass $obj */
        $obj = self::search_class(
          referent: $classRight,
          classname: $classname,
          classesAllreadyFound: $classesAllreadyFound,
          depth: $depth+1
        );
        if(null !== $obj)
          return $obj;
      }
    }
    return null;
  }

  public function getName(): string {
    return ucfirst(parent::getName());
  }

  /**
   * Récupération des associations
   *
   * @return array<int,UnityAssociationEnd>
   */
  public function getAssociationEnds(): array {
    /** @var array<int, UnityAssociationEnd> $associationEnds */
    $associationEnds = $this->_associations;
    /** @var array<int, UnityAssociationEnd> $newOutput */
    $newOutput = [];
    if(count($this->_associations)) {
      $collection = new Collection(
        $this->_associations,
        function(int $index, UnityAssociationEnd $associationEnd): string { return $associationEnd->getName(); },
        function(string $keyA, string $keyB): bool { return (strcmp($keyA,$keyB)<0); },
        function(int $index, UnityAssociationEnd $associationEnd): UnityAssociationEnd { return $associationEnd; }
      );
      $collection->heapSort();

      for($i=0;$i< $collection->count();$i++)
        $newOutput[]=$collection->get($i);
    }
    return $newOutput;
  }

  /**
   * Récupération des associations à la droite de la classe
   *
   * @return array<int,UnityAssociationEnd>
   */
  public function getAssociationRgtEnds(): array {
    /** @var array $output */
    $output=[];
    /** @var array<int, UnityAssociationEnd> $assocEnds */
    $assocEnds = $this->getAssociationEnds();
    /**
     * @var int $index
     * @var UnityAssociationEnd $assocEnd
     */
    foreach($assocEnds as $index => $assocEnd) {
      /** @var UnityClass $classLft */
      $classLft = $assocEnd
        ->getAssociation()
        ->getAssociationLft()
        ->getClass()
      ;
      if($classLft->getName() === $this->getName())
        $output[$index]= $assocEnd;
    }
    return $output;
  }

  /**
   * Récupération des associations à la gauche de la classe
   *
   * @return array<int,UnityAssociationEnd>
   */
  public function getAssociationLftEnds(): array {
    /** @var array $output */
    $output=[];
    /** @var array<int, UnityAssociationEnd> $assocEnds */
    $assocEnds = $this->getAssociationEnds();
    /**
     * @var int $index
     * @var UnityAssociationEnd $assocEnd
     */
    foreach($assocEnds as $index => $assocEnd) {
      /** @var UnityClass $classLft */
      $classRgt = $assocEnd
        ->getAssociation()
        ->getAssociationRgt()
        ->getClass()
      ;
      if($classRgt->getName() === $this->getName())
        $output[$index]= $assocEnd;
    }
    return $output;
  }

  public function addAssociationEnd(UnityAssociationEnd $associationEnd): self {
    /** @var UnityAssociationEnd $associationLocalEnd */
    foreach($this->_associations as $associationLocalEnd)
      if($associationLocalEnd->getName() === $associationEnd->getName())
        return $this;

    $this->_associations[]=$associationEnd;
    $associationEnd->setClass($this);
    return $this;
  }

  public function removeAssociationEnd(UnityAssociationEnd $associationEnd): self {
    /** @var string $associationEndName */
    $associationEndName = $associationEnd->getName();
    foreach($this->_associations as $index => $lAssociationEnd) {
      if($lAssociationEnd->getName() === $associationEndName) {
        unset($this->_associations[$index]);
        unset($associationEnd);
      }
    }
    return $this;
  }

  /**
   * Récupération d'un attribut par son nom
   *
   * @param string $name
   * @return ?UnityAttribute
   */
  public function getAttribute(string $name): ?UnityAttribute {
    foreach($this->_attributes as $attr) {
      if($attr->getName() === $name)
        return $attr;
      if($attr->getOriginName() === $name)
        return $attr;
    }
    return null;
  }

  /**
   * Récupération des attributs d'une classe
   *
   * @return array<int, UnityAttribute>
   */
  public function getAttributes(): array {
    $newOutput = [];
    if(count($this->_attributes)) {
      /** @todo trier via countingSort */
      $collection = new Collection(
        $this->_attributes,
        function(int $index, UnityAttribute $attribute): string { return $attribute->getName(); },
        /** Fonction de comparaison pour le tri */
        function(string $keyA, string $keyB): bool { return (strcmp($keyA,$keyB)<0); },
        function(int $index, UnityAttribute $attribute): UnityAttribute { return $attribute; }
      );
      $collection->heapSort();

      for($i=0;$i< $collection->count();$i++)
        $newOutput[]=$collection->get($i);
    }
    return $newOutput;
  }

  public function addAttribute(UnityAttribute $attribute): self {
    $this->_attributes[]=$attribute;
    $attribute->setClass($this);
    return $this;
  }

  public function toArray(): array {
    /** @var array $output */
    $output=[
      'name' => $this->getName(),
      'attributes' => [],
      'associations' => []
    ];

    /**
     * Récupération des attributs de la classe
     *
     * @var UnityAttribute $attr
     */
    foreach($this->getAttributes() as $attr) {
      $output['attributes'][$attr->getName()] = [
        "type" => $attr->getType(),
        "nullable" => $attr->isNullable(),
      ];
    }

    /**
     * Récupération des attributs à la droite de la classe
     *
     * @var UnityAssociationEnd $ae
     */
    foreach($this->getAssociationEnds() as $ae) {
      /** @var UnityAssociation $association */
      $association = $ae->getAssociation();
      $lftAssociation = $association->getLftClassname();
      if($lftAssociation === $this->getName()) {
        $re = $association->getAssociationRgt();
        $classRgt = $re->getClass();
        $rgt = $classRgt->toArray();
        $output['associations'][$association->getName()] = [
          "multiplicityLft" => $ae->getMultiplicity(),
          "multiplicityRgt" => $re->getMultiplicity(),
          "class" => $rgt,
        ];
      }
    }
    return $output;
  }

  /**
   * Génération de la représentation d'une instance en prenant en compte
   * sa déclaration primitive.
   *
   * @param array $data
   * @return
   */
  public function generate_instance(array $data) {
    dump($data);
    die;
  }
}
