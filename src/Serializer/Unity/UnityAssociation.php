<?php

namespace FOPG\Component\MOFBundle\Serializer\Unity;

use FOPG\Component\MOFBundle\Contracts\Unity\UnityAssociationEndInterface;
use FOPG\Component\MOFBundle\Contracts\Unity\UnityAssociationInterface;
use FOPG\Component\MOFBundle\Exception\Unity\InconsistencyAssociationException;
use FOPG\Component\UtilsBundle\String\StringFacility;

class UnityAssociation extends AbstractUnityCommon implements UnityAssociationInterface
{
  private ?UnityAssociationEnd $_associationLft = null;
  private ?UnityAssociationEnd $_associationRgt = null;

  public function __construct(string $name) {
    parent::__construct($name);
  }

  public function getName(): string {
    return ucfirst(parent::getName());
  }

  public static function merge(UnityAssociation $a, UnityAssociation $b, bool &$newVersionRequired=false): void {
    if($a->getName() !== $b->getName())
      throw new InconsistencyAssociationException($a,$b);
    $tmp=false;
    $tmp2=false;
    UnityAssociationEnd::merge($a->getAssociationLft(), $b->getAssociationLft(), $tmp);
    UnityAssociationEnd::merge($a->getAssociationRgt(), $b->getAssociationRgt(), $tmp2);
    $newVersionRequired = $tmp||$tmp2;
  }

  /**
   * @param UnityAssociation $association
   * @param UnityClass $copyLft
   * @param UnityClass $copyRgt
   * @return UnityAssociation
   */
  public static function copy(UnityAssociation $association, UnityClass $copyLft, UnityClass $copyRgt): UnityAssociation {
    $copyAssociation = new UnityAssociation($association->getOriginName());
    $copyAssociationEndLeft = UnityAssociationEnd::copy($association->getAssociationLft(), $copyLft);
    $copyLft->addAssociationEnd($copyAssociationEndLeft);
    $copyAssociation->setAssociationLft($copyAssociationEndLeft);
    $copyAssociationEndRight = UnityAssociationEnd::copy($association->getAssociationRgt(), $copyRgt);
    $copyRgt->addAssociationEnd($copyAssociationEndRight);
    $copyAssociation->setAssociationRgt($copyAssociationEndRight);
    return $copyAssociation;
  }

  /**
   * @param UnityAssociation $a
   * @param UnityAssociation $b
   * @return bool
   */
  public static function equals(UnityAssociation $a, UnityAssociation $b): bool {
    $check = true;
    $check = $check && ($a->getName() === $b->getName());
    $check = $check && UnityAssociationEnd::equals($a->getAssociationRgt(), $b->getAssociationRgt());
    $check = $check && UnityAssociationEnd::equals($a->getAssociationLft(), $b->getAssociationLft());
    return $check;
  }

  /**
   * Récupération du déclarant de classe à gauche
   *
   * @return ?string
   */
  public function getLftClassname(): ?string {
    /** @var ?UnityAssociationEnd $associationLft */
    $associationLft = $this->getAssociationLft();
    /** @var ?UnityClass $classLft */
    $classLft = $associationLft ? $associationLft->getClass() : null;
    return $classLft ? $classLft->getName() : null;
  }

  /**
   * Récupération du déclarant de classe à droite
   *
   * @return ?string
   */
  public function getRgtClassname(): ?string {
    /** @var ?UnityAssociationEnd $associationRgt */
    $associationRgt = $this->getAssociationRgt();
    /** @var ?UnityClass $classRgt */
    $classRgt = $associationRgt ? $associationRgt->getClass() : null;
    return $classRgt ? $classRgt->getName() : null;
  }

  public function setAssociationLft(UnityAssociationEnd $lft): self {
    $this->_associationLft=$lft;
    $lft->setAssociation($this);
    return $this;
  }

  public function getAssociationLft(): ?UnityAssociationEnd {
    return $this->_associationLft;
  }

  public function setAssociationRgt(UnityAssociationEnd $rgt): self {
    $this->_associationRgt=$rgt;
    $rgt->setAssociation($this);
    return $this;
  }

  public function getAssociationRgt(): ?UnityAssociationEnd {
    return $this->_associationRgt;
  }

  public static function removeAssociation(UnityAssociation $association): void {
    /** @var ?UnityAssociationEnd $associationLft */
    $associationLft = $association->getAssociationLft();
    /** @var ?UnityClass $lft */
    $lft = $associationLft ? $associationLft->getClass() : null;
    /** @var ?UnityAssociationEnd $associationRgt */
    $associationRgt = $association->getAssociationRgt();
    /** @var ?UnityClass $rgt */
    $rgt = $associationRgt ? $associationRgt->getClass() : null;
    if(null !== $lft)
      $lft->removeAssociationEnd($associationLft);
    if(null !== $rgt)
      $rgt->removeAssociationEnd($associationRgt);
    unset($association);
  }

  /**
   * Génération d'une association entre deux classes
   *
   * @param UnityClass $rgt
   * @param UnityClass $lft
   * @param string $multiplicity
   * @param ?string $associationName Nom de l'association courante
   */
  public static function generateAssociation(
    UnityClass $lft,
    UnityClass $rgt,
    string $multiplicity,
    ?string $associationName=null
  ): void {
    /** @var string $nameLft */
    $nameLft = StringFacility::toSnakeCase($lft->getName());
    /** @var string $nameRgt */
    $nameRgt = StringFacility::toSnakeCase($rgt->getName());
    /** @var string $associationName */
    $associationName = StringFacility::toSnakeCase($associationName ?? $nameRgt.'_'.$nameLft);
    /** @var UnityAssociation $association */
    $association = new UnityAssociation($associationName);

    $multiplicityLft= UnityAssociationEnd::ZERO_TO_ONE;
    if(in_array($multiplicity,[self::MANY_TO_MANY, self::MANY_TO_ONE])) {
      $multiplicityLft = UnityAssociationEnd::ZERO_TO_MANY;
    }

    /** @var AssociationEnd $associationLft */
    $associationLft = new UnityAssociationEnd($nameRgt);
    /** @var string $multiplicityLft */
    $associationLft->setMultiplicity($multiplicityLft);
    $lft->addAssociationEnd($associationLft);
    $association->setAssociationLft($associationLft);

    /** @var string $multiplicityRgt */
    $multiplicityRgt= ($multiplicity === UnityAssociationEndInterface::ONE_TO_ONE) ? UnityAssociationEnd::ONE_TO_ONE : UnityAssociationEnd::ZERO_TO_ONE;
    if(in_array($multiplicity,[self::ONE_TO_MANY, self::MANY_TO_MANY])) {
      $multiplicityRgt = UnityAssociationEnd::ZERO_TO_MANY;
    }

    /** @var UnityAssociationEnd $associationRgt */
    $associationRgt = new UnityAssociationEnd($nameLft);
    $associationRgt->setMultiplicity($multiplicityRgt);
    $rgt->addAssociationEnd($associationRgt);
    $association->setAssociationRgt($associationRgt);

    /**
     * Contrôle d'intégrité des espaces de nom
     *
     */
    UnityNamespace::merge_namespace($lft, $rgt);
  }
}
