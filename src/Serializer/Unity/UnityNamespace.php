<?php

namespace FOPG\Component\MOFBundle\Serializer\Unity;

use FOPG\Component\MOFBundle\Exception\Unity\CollisionNamespaceException;
use FOPG\Component\UtilsBundle\Collection\Collection;
use FOPG\Component\UtilsBundle\String\StringFacility;

class UnityNamespace extends AbstractUnityCommon
{
  /** @var Array<int, UnityClass> */
  private array $_classes = [];

  public function __construct(string $name) {
    parent::__construct($name);
  }

  /**
   * @param string $name
   * @return ?UnityClass
   */
  public function findClass(string $name): ?UnityClass {
    /** @var UnityClass $class */
    foreach($this->getClasses() as $class)
      if($class->getName() === $name)
        return $class;
    return null;
  }

  /**
   * Renvoi des classes référencées
   *
   * @return array<string, int>
   */
  public function findClasses(): array {
    /** @var array<string, int> */
    $classNames = [];
    array_walk($this->_classes, function(UnityClass $item)use(&$classNames) {
      $classNames[$item->getName()]=0;
      /** @var array<string, int> */
      $subClasses = $item->findClasses();
      foreach($subClasses as $subClassName => $eval) {
        $oldEval = $classNames[$subClassName] ?? 0;
        $classNames[$subClassName]=max($oldEval,$eval);
      }
    });
    return $classNames;
  }

  /**
   * Récupération de l'ensemble des associations à l'intérieur du réseau de classe
   *
   * @return array<int,string>
   */
  public function findAssociations(): array {
    $associations=[];
    foreach($this->_classes as $class)
      foreach($class->getAssociationEnds() as $as)
        $associations[]=$as->getAssociation()->getName();
    return array_unique($associations);
  }

  /**
   * Récupération d'une association à l'intérieur du réseau de classe
   *
   * @param string $name
   * @return ?UnityAssociation
   */
  public function findAssociation(string $name): ?UnityAssociation {
    foreach($this->_classes as $class)
      foreach($class->getAssociationEnds() as $as)
        if($as->getAssociation()->getName() === $name)
          return $as->getAssociation();
    return null;
  }

  /**
   * Fusion de deux espaces de nom
   *
   * @param UnityNamespace $a
   * @param UnityNamespace $b
   * @param bool $newVersionRequired
   * @return void
   */
  public static function merge(UnityNamespace $a, UnityNamespace $b, bool &$newVersionRequired=false): void {
    if($a->getName() !== $b->getName())
      throw new CollisionNamespaceException($a,$b);

    /** @var array<string, int> $classNames */
    $classNames = $a->findClasses();

    /**
     * Fusion de l'ensemble des classes entre $a et $b
     *
     */
    foreach(array_keys($classNames) as $className) {
      /** @var UnityClass $classA */
      $classA = $a->findClass($className);
      /** @var ?UnityClass $classB */
      $classB = $b->findClass($className);
      if(null!==$classB) {
        $tmp = false;
        UnityClass::merge($classA, $classB, $tmp);
        $newVersionRequired = ($newVersionRequired||$tmp);
      }
    }

    /**
     * Ajout des classes présentes dans $b et manquantes dans $a
     *
     */
    /** @var array<string, int> $classNames */
    $classNames = $b->findClasses();
    /** @var string $className */
    foreach(array_keys($classNames) as $className) {
      /** @var UnityClass $classA */
      $classA = $a->findClass($className);
      /** @var ?UnityClass $classB */
      $classB = $b->findClass($className);
      if(null===$classA) {
        UnityClass::copy($classB, $a);
        $newVersionRequired = true;
      }
    }

    /**
     * Fusion des associations
     *
     */
    /** @var array<int, string> $associationNames */
    $associationNames = $a->findAssociations();
    foreach($associationNames as $associationName) {
      /** @var UnityAssociation $associationA */
      $associationA = $a->findAssociation($associationName);
      /** @var ?UnityAssociation $associationB */
      $associationB = $b->findAssociation($associationName);
      if(null !== $associationB)
        UnityAssociation::merge($associationA, $associationB);
    }

    /** @var array<int, string> $associationNames */
    $associationNames = $b->findAssociations();

    foreach($associationNames as $associationName) {
      /** @var UnityAssociation $associationA */
      $associationA = $a->findAssociation($associationName);
      /** @var ?UnityAssociation $associationB */
      $associationB = $b->findAssociation($associationName);
      if(null === $associationA) {
        /** @var string $classBLftName */
        $classBLftName = $associationB
          ->getAssociationLft()
          ->getClass()
          ->getName()
        ;
        /** @var UnityClass $classLft */
        $classLft = $a->findClass($classBLftName);

        /** @var string $classBRgtName */
        $classBRgtName = $associationB
          ->getAssociationRgt()
          ->getClass()
          ->getName()
        ;
        /** @var UnityClass $classRgt */
        $classRgt = $a->findClass($classBRgtName);

        $copyAssociation = UnityAssociation::copy($associationB, $classLft, $classRgt);
      }
    }
  }

  /**
   * Copie de l'espace de nom
   *
   * @param UnityNamespace $original
   * @return UnityNamespace
   */
  public static function copy(UnityNamespace $original): UnityNamespace {
    $copy = new UnityNamespace($original->getOriginName());
    return $copy;
  }

  public static function equals(?UnityNamespace $a, ?UnityNamespace $b): bool {
    $check = true;
    if((null===$a) && (null===$b))
      return true;
    elseif((null===$a) && (null!==$b))
      return false;
    elseif((null!==$a) && (null===$b))
      return false;
    else {
      $check = $check && ($a->getOriginName() === $b->getOriginName());
    }

    /** @var UnityClass $aClass */
    foreach($a->getClasses() as $aClass) {
      /** @var UnityClass $bClass */
      $bClass = $b->findClass($aClass->getName());
      $check = $check && UnityClass::equals($aClass, $bClass);
    }

    /** @var UnityClass $bClass */
    foreach($b->getClasses() as $bClass) {
      /** @var UnityClass $aClass */
      $aClass = $a->findClass($bClass->getName());
      $check = $check && UnityClass::equals($aClass, $bClass);
    }

    /** @var string $associationName */
    foreach($a->findAssociations() as $associationName) {
      /** @var ?UnityAssociation $bAssociation */
      $bAssociation = $b->findAssociation($associationName);
      /** @var ?UnityAssociation $aAssociation */
      $aAssociation = $a->findAssociation($associationName);
      $check = $check && UnityAssociation::equals($aAssociation, $bAssociation);
    }

    /** @var string $associationName */
    foreach($b->findAssociations() as $associationName) {
      /** @var ?UnityAssociation $bAssociation */
      $bAssociation = $b->findAssociation($associationName);
      /** @var ?UnityAssociation $aAssociation */
      $aAssociation = $a->findAssociation($associationName);
      $check = $check && UnityAssociation::equals($aAssociation, $bAssociation);
    }
    return $check;
  }

  public static function duplicate(UnityNamespace $original): UnityNamespace {
    /** @var UnityNamespace $copy */
    $copy = UnityNamespace::copy($original);
    /** @var array<string, int> $classNames */
    $classNames = $original->findClasses();

    /**
     * Copie de l'ensemble des classes
     *
     */
    foreach(array_keys($classNames) as $className) {
      /** @var UnityClass $class */
      $class = $original->findClass($className);
      $copyClass = UnityClass::copy($class, $copy);
    }

    /**
     * Copie de l'ensemble des associations
     *
     */
    /** @var string $associationName */
    foreach($original->findAssociations() as $associationName) {
      /** @var UnityAssociation $association */
      $association = $original->findAssociation($associationName);
      /** @var string $classnameLft */
      $classnameLft = $association->getAssociationLft()->getClass()->getName();
      /** @var UnityClass $copyLft */
      $copyLft = $copy->findClass($classnameLft);
      /** @var string $classnameRgt */
      $classnameRgt = $association->getAssociationRgt()->getClass()->getName();
      /** @var UnityClass $copyRgt */
      $copyRgt = $copy->findClass($classnameRgt);
      /** @var UnityAssociation $copyAssociation */
      $copyAssociation = UnityAssociation::copy($association, $copyLft, $copyRgt);
    }
    return $copy;
  }

  /**
   * @return array<int, UnityClass>
   */
  public function getClasses(): array {
    return $this->_classes;
  }

  /**
   * @param UnityClass $class
   * @return self
   */
  public function addClass(UnityClass $class): self {
    /** @var UnityClass $uClass */
    foreach($this->_classes as $uClass)
      if($uClass->getName() === $class->getName())
        return $this;
    $this->_classes[]=$class;
    return $this;
  }

  /**
   * @return string
   */
  public function getName(): string {
    $name = parent::getName();
    return ucfirst($name);
  }

  /**
   * @param UnityClass $lft
   * @param UnityClass $rgt
   * @return void
   */
  public static function merge_namespace(UnityClass $lft, UnityClass $rgt): void {
    /** @var ?UnityNamespace $namespaceLft */
    $namespaceLft = $lft->getNamespace();
    /** @var ?UnityNamespace $namespaceRgt */
    $namespaceRgt = $rgt->getNamespace();
    if((null!==$namespaceLft)&&(null===$namespaceRgt))
      $rgt->setNamespace($namespaceLft);
    elseif((null===$namespaceLft)&&(null!==$namespaceRgt))
      $lft->setNamespace($namespaceRgt);
    if((null!==$namespaceLft)&&(null!==$namespaceRgt))
      if((($namespaceLft->getName())!==$namespaceRgt->getName()))
        throw new CollisionNamespaceException($namespaceLft, $namespaceRgt);
  }
}
