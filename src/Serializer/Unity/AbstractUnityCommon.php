<?php

namespace FOPG\Component\MOFBundle\Serializer\Unity;

use FOPG\Component\UtilsBundle\String\StringFacility;
use FOPG\Component\MOFBundle\Contracts\Unity\AbstractUnityCommonInterface;

abstract class AbstractUnityCommon implements AbstractUnityCommonInterface
{
  private ?string $_name = null;
  private ?string $_originName = null;

  public function getName(): ?string {
    return StringFacility::toCamelCase($this->_name);
  }

  public function getOriginName(): ?string {
    return $this->_originName;
  }

  public function __construct(string $name) {
    $this->_name = StringFacility::toSnakeCase($name);
    $this->_originName = $name;
  }
}
