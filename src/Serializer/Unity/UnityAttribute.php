<?php

namespace FOPG\Component\MOFBundle\Serializer\Unity;

use FOPG\Component\MOFBundle\Contracts\Unity\UnityAttributeInterface;
use FOPG\Component\MOFBundle\Exception\Unity\InconsistencyAttributeException;
use FOPG\Component\UtilsBundle\String\StringFacility;

class UnityAttribute extends AbstractUnityCommon implements UnityAttributeInterface
{
  private ?string $_type = null;
  private ?UnityClass $_class = null;
  private array $_options = [];
  private bool $_nullable = false;
  private bool $_partOfHash = false;
  private ?string $_fullName = null;

  /**
   * Fonction de copie d'un attribut
   *
   * @param UnityAttribute $attr
   * @return UnityAttribute
   */
  public static function copy(UnityAttribute $attr): UnityAttribute {
    $copy = new UnityAttribute($attr->getOriginName());
    $copy->setType($attr->getType());
    foreach($attr->getOptions() as $key => $val)
      $copy->addOption($key, $val);
    $copy->setNullable($attr->isNullable());
    $copy->setPartOfHash($attr->isPartOfHash());

    $class = $attr->getClass();
    if(null !== $class)
      $copy->setFullName($attr->getName(), $class->getName());

    return $copy;
  }

  /**
   * Fusion d'attribut
   *
   * Le résultat de la fusion se retrouve dans le premier paramètre.
   *
   * GESTION DES OPTIONS:
   * - Toute option manquante dans $attrA présente dans $attrB sera recopiée dans $attrA
   * - Dans l'éventualité ou $attrA et $attrB ont une même option mais avec une information différente, alors
   *   la valeur de $attrB écrase celle de $attrA
   * GESTION DU NULLABLE:
   * - Le nullable sera à true si $attrA ou $attrB sont à true
   * GESTION DU TYPE:
   * - Le type est surchargé par $attrB
   * GESTION DE LA PARTIE DE HASH:
   * - SI au moins l'un des deux est une partie de hash ET que l'attribut n'est pas nul,
   *   ALORS l'attribut fusionné sera une partie de hash
   *
   * Remontée d'une montée de version : une version sera relevée si :
   * 1. Le type de l'attribut change
   * 2. la nullité de l'attribut change
   * 3. une inclusion dans la clé de hashage est relevée
   *
   * @param UnityAttribute $attrA
   * @param UnityAttribute $attrB
   * @param bool $newVersionRequired
   * @return void
   */
  public static function merge(
    UnityAttribute $attrA,
    UnityAttribute $attrB,
    bool &$newVersionRequired=false
  ): void {
    if($attrA->getName() !== $attrB->getName())
      throw new InconsistencyAttributeException($attrA,$attrB);
    self::merge_nullable_and_part_of_hash($attrA, $attrB,$newVersionRequired);
    self::merge_type($attrA, $attrB,$newVersionRequired);
    self::merge_options($attrA, $attrB);
  }

  /**
   * Sous fonction de gestion du nullable et du partOfHash
   *
   * @param UnityAttribute $attrA
   * @param UnityAttribute $attrB
   * @param bool $newVersionRequired
   * @return void
   */
  private static function merge_nullable_and_part_of_hash(
    UnityAttribute $attrA,
    UnityAttribute $attrB,
    bool &$newVersionRequired=false
  ): void {
    $newVersionRequired = ($attrA->isNullable() !== $attrB->isNullable());
    /** @var bool $isNullable */
    $isNullable = $attrA->isNullable()||$attrB->isNullable();
    $attrA->setNullable($isNullable);
    /** @var bool $oldPartOfHash */
    $oldPartOfHash = $attrA->isPartOfHash();
    $newPartOfHash =
      (false === $isNullable)
      &&
      (
        $oldPartOfHash
        ||
        $attrB->isPartOfHash()
      )
    ;
    if($oldPartOfHash !== $newPartOfHash) {
      $attrA->setPartOfHash($newPartOfHash);
      $newVersionRequired=true;
    }

  }

  /**
   * Sous fonction de gestion de la fusion des types
   *
   * @param UnityAttribute $attrA
   * @param UnityAttribute $attrB
   * @return void
   */
  private static function merge_type(
    UnityAttribute $attrA,
    UnityAttribute $attrB,
    bool &$newVersionRequired=false
  ): void {
    $typeA = $attrA->getType();
    $typeB = $attrB->getType();

    if($typeA !== $typeB) {
      $type = in_array(UnityAttribute::TYPE_TEXT, [$typeA, $typeB]) ? UnityAttribute::TYPE_TEXT : UnityAttribute::TYPE_STRING;
      $attrA->setType($type);
      $newVersionRequired=true;
    }
  }

  /**
   * Sous fonction de gestion de la fusion des options
   *
   * @param UnityAttribute $attrA
   * @param UnityAttribute $attrB
   * @return void
   */
  private static function merge_options(
    UnityAttribute $attrA,
    UnityAttribute $attrB
  ): void {

    $maxlengthA = $attrA->getOption(UnityAttribute::OPTION_STRING_MAXLENGTH);
    $maxlengthB = $attrB->getOption(UnityAttribute::OPTION_STRING_MAXLENGTH);
    if(null !== $maxlengthB) {
      $attrA->addOption(
        UnityAttribute::OPTION_STRING_MAXLENGTH,
        max($maxlengthA, $maxlengthB)
      );
    }

    foreach($attrB->getOptions() as $key => $val) {
      if(!in_array($key, [UnityAttribute::OPTION_STRING_MAXLENGTH]))
        $attrA->addOption($key, $val);
    }

    if(UnityAttribute::TYPE_TEXT === $attrA->getType()) {
      $attrA->removeOption(UnityAttribute::OPTION_STRING_MAXLENGTH);
    }
  }

  /**
   * Comparateur d'équivalence entre deux attributs distinct
   *
   * @param UnityAttribute $attrA
   * @param UnityAttribute $attrB
   * @return bool
   */
  public static function equals(?UnityAttribute $attrA, ?UnityAttribute $attrB): bool {
    $check = true;
    if((null===$attrA) && (null!==$attrB))
      return false;
    if((null!==$attrA) && (null===$attrB))
      return false;

    $check = $check && ($attrA->getName() === $attrB->getName());
    $check = $check && ($attrA->getType() === $attrB->getType());
    $check = $check && ($attrA->isNullable() === $attrB->isNullable());
    $check = $check && ($attrA->isPartOfHash() === $attrB->isPartOfHash());
    $check = $check && ($attrA->getFullName() === $attrB->getFullName());
    foreach($attrA->getOptions() as $key => $val)
      $check = $check && ($attrB->getOption($key) === $val);
    foreach($attrB->getOptions() as $key => $val)
      $check = $check && ($attrA->getOption($key) === $val);
    return $check;
  }

  /**
   * @param string $name
   * @param string $className
   * @return string
   */
  public static function normalizeFullName(string $name, string $className): string {
    /** @var string $regexp */
    $regexp = '(^'.preg_quote($className).'|'.preg_quote($className).'$)';
    $name = preg_replace("/$regexp/i", "", $name);
    /** @var string $fullName */
    $fullName = $name.$className;
    return StringFacility::toCamelCase(
      StringFacility::toSnakeCase($fullName)
    );
  }

  public function getSmallName(): string {
    $className = $this->getClass() ? $this->getClass()->getName() : null;
    if(null !== $className) {
      /** @var string $regexp */
      $regexp = preg_quote($className).'$';
      return preg_replace("/$regexp/i", "", $this->getFullName());
    }
    return $this->getFullName();
  }

  public function __construct(string $name, string $type=UnityAttributeInterface::TYPE_STRING) {
    $this->_type = $type;
    parent::__construct($name);
  }

  /**
   * @param string $name
   * @param string $className
   * @return self
   */
  public function setFullName(string $name, string $className): self {
    $this->_fullName = self::normalizeFullName($name, $className);
    return $this;
  }

  /**
   * @return ?string
   */
  public function getFullName(): string {
    return $this->_fullName ?? $this->getName();
  }

  public function setNullable(bool $nullable): self {
    $this->_nullable = $nullable;
    return $this;
  }

  public function isNullable(): bool {
    return $this->_nullable;
  }

  public function getOption(string $option): mixed {
    return $this->_options[$option] ?? null;
  }

  public function getOptions(): array {
    return $this->_options;
  }

  public function addOption(string $key, mixed $value): self {
    $this->_options[$key] = $value;
    return $this;
  }

  public function removeOption(string $key): self {
    unset($this->_options[$key]);
    return $this;
  }

  public function setType(string $type): self {
    $this->_type = $type;
    return $this;
  }

  public function getType(): ?string {
    return $this->_type;
  }

  public function setClass(UnityClass $class): self {
    $this->_class = $class;
    $this->setFullName($this->getName(), $class->getName());
    return $this;
  }

  public function getClass(): ?UnityClass {
    return $this->_class;
  }

  public function setPartOfHash(bool $partOfHash): self {
    $this->_partOfHash = $partOfHash;
    return $this;
  }

  public function isPartOfHash(): bool {
    return $this->_partOfHash;
  }
}
