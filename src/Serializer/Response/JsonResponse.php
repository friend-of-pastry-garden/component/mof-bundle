<?php

namespace FOPG\Component\MOFBundle\Serializer\Response;

use FOPG\Component\MOFBundle\Contracts\Response\ResponseInterface;
use Symfony\Component\HttpFoundation\JsonResponse as SfJsonResponse;

class JsonResponse extends SfJsonResponse implements ResponseInterface
{

}
