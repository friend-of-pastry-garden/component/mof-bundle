<?php

namespace FOPG\Component\MOFBundle\Serializer\Response;

use FOPG\Component\MOFBundle\Contracts\Response\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as SfResponse;

class Response extends SfResponse implements ResponseInterface
{

}
