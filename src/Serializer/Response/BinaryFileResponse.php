<?php

namespace FOPG\Component\MOFBundle\Serializer\Response;

use FOPG\Component\MOFBundle\Contracts\Response\ResponseInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse as SfBinaryFileResponse;

class BinaryFileResponse extends SfBinaryFileResponse implements ResponseInterface
{

}
