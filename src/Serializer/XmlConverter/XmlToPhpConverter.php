<?php

namespace FOPG\Component\MOFBundle\Serializer\XmlConverter;

use FOPG\Component\MOFBundle\Serializer\Unity\UnityClass;
use FOPG\Component\UtilsBundle\Collection\Collection;
use FOPG\Component\UtilsBundle\String\StringFacility;

trait XmlToPhpConverter {

  /**
   * @param array<int,\SimpleXMLElement> $elements
   * @param UnityClass $class
   * @param array $memory
   */
  public static function get_stats_from_xml(array $elements, UnityClass $class, array &$memory=[]): array {
    /** @var int $cpt */
    $cpt = count($elements);
    $output = [];
    /** @var UnityAttribute $attribute */
    foreach($class->getAttributes() as $attribute) {
      /** @var string $originAttrName */
      $originAttrName = $attribute->getOriginName();
      $output[$originAttrName]=[
        'cpt' => 0,
        'nullable' => $attribute->isNullable(),
        'name' => $attribute->getName(),
        'cptOfDate' => 0,
        'cptOfInt' => 0,
        'type' => $attribute->getType(),
        'isTxt' => false,
        'choices' => $attribute->getOption('choices') ?? [],
        'maxlength' => 0,
        'partOfHash' => $attribute->isPartOfHash(),
      ];
    }

    /** @var \SimpleXMLElement $element */
    foreach($elements as $element) {
      /** @var UnityAttribute $attribute */
      foreach($class->getAttributes() as $attribute) {
        /** @var string $originAttrName */
        $originAttrName = $attribute->getOriginName();
        /** @var string $obj */
        $obj = trim((string)$element->{$originAttrName});
        if($obj) {
          $output[$originAttrName]['cpt']++;
          $maxlength = mb_strlen($obj);
          if($maxlength > $output[$originAttrName]['maxlength'])
            $output[$originAttrName]['maxlength']=$maxlength;
        }
        /** @var ?\DateTime $date */
        $date = StringFacility::toDate($obj);
        if(null !== $date)
          $output[$originAttrName]['cptOfDate']++;
        /** @var ?int $intVal */
        $intVal = StringFacility::toInt($obj);
        if(null !== $intVal)
          $output[$originAttrName]['cptOfInt']++;
        /** @var bool $isTxt */
        $isTxt = StringFacility::isText($obj);
        if(true === $isTxt)
          $output[$originAttrName]['isTxt']=true;
        if(
          $obj &&
          !in_array($obj,$output[$originAttrName]['choices']) &&
          (count($output[$originAttrName]['choices']) < self::MINIMUM_ITEMS_FOR_SIGNIFIANT)
        )
          $output[$originAttrName]['choices'][]=$obj;
      }
      unset($element);
    }
    unset($elements);

    foreach($output as $originAttrName => $informations) {

      $tmp = $class->getAttribute($informations['name']);

      /**
      * 1. Gestion du cas où l'attribut peut être nul
      *
      */
      if($informations['cpt'] !== $cpt) {
        $tmp->setNullable(true);
      }
      $tmp->setPartOfHash($informations['cpt'] === $cpt);

      /**
       * 2. Gestion du cas l'attribut est une date
       *
       */
      if($informations['cpt'] === $informations['cptOfDate']) {
        $tmp->setType($tmp::TYPE_DATE);
      }

      /**
       * 3. Gestion du cas l'attribut est un entier
       *
       */
      if($informations['cpt'] === $informations['cptOfInt']) {
        $tmp->setType($tmp::TYPE_INTEGER);
      }

      /**
       * 4. Gestion du cas ou l'attribut est un texte
       *
       */
      if(true === $informations['isTxt']) {
        $tmp->setType($tmp::TYPE_TEXT);
      }


      if($informations['cpt'] > self::MINIMUM_ITEMS_FOR_SIGNIFIANT && $tmp::TYPE_STRING === $tmp->getType()) {
        /**
         * 5. Gestion du cas ou l'attribut est un booléen
         *
         */
        if(count($informations['choices'])<=$tmp::MAXSIZE_ENUM) {
          sort($informations['choices']);
          $tmp->setType($tmp::TYPE_ENUM);
          $tmp->addOption('choices', $informations['choices']);
        }
        else {
          /**
          * 6. Enrichissement du cas varchar
          *
          */
          $tmp->addOption('maxlength', $informations['maxlength']);
        }
      }

    }

    return $output;
  }
}
