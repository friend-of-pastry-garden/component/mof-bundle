<?php

namespace FOPG\Component\MOFBundle\Serializer\XmlConverter;

use FOPG\Component\UtilsBundle\Collection\Collection;

trait XmlToDtdConverter {

  private static array $_known_xpath=[];

  /**
   * Fonction d'appel pour produire une DTD à partir d'un XML
   *
   * La génération est très limitative par rapport à l'exhaustivité d'une
   * DTD ordinaire. L'objectif reste de produire une DTD suffisante pour
   * que la transformation vers Unity soit satisfaisante.
   *
   * @param ?string $xmlFilename Nom du fichier XML d'alimentation
   * @return string Nom du fichier DTD de sortie
   */
  public static function generate_dtd_from_xml(?string $filename): string {
    self::$_known_xpath=[];
    /** @var \SimpleXMLElement $xml */
    $root    = \simplexml_load_file($filename);
    $rootNodeName = $root->getName();
    self::launch_child_sub_information(xpath: "/$rootNodeName", elementName: $rootNodeName,element: $root);

    /** @todo génération de la DTD à partir du tableau self::$_known_xpath */
    $data = self::generate_elements($filename);
    $tmpFile = sys_get_temp_dir().'/'.uniqid().'.dtd';
    file_put_contents($tmpFile, $data);
    return $tmpFile;
  }

  public static function convert_array_to_string(array $known_xpath): string {
    $element  = $known_xpath['element'];
    $isValue  = $known_xpath['is_value'];
    $nbTab    = max(count(explode('/',$known_xpath['xpath']))-2,0);
    if(false === $isValue) {
      $associations=[];
      foreach($known_xpath['associations']['ZeroToOne'] as $assoc)
        $associations[]=$assoc.'?';
      foreach($known_xpath['associations']['StrictlyOne'] as $assoc)
        $associations[]=$assoc;
      foreach($known_xpath['associations']['ZeroToMany'] as $assoc)
        $associations[]=$assoc;
      foreach($known_xpath['associations']['OneToMany'] as $assoc)
        $associations[]=$assoc.'+';
      return str_repeat("\t",$nbTab)."<!ELEMENT $element (".implode(",",$associations).")>";
    }
    else
      return str_repeat("\t",$nbTab)."<!ELEMENT $element (#PCDATA)>";
  }

  public static function generate_elements(string $filename): string {
    $now = new \DateTime();
    $strNow = $now->format("d/m/Y");
    /** @var array $fileParts */

    $fileParts = explode("/",$filename);
    $filename = $fileParts[count($fileParts)-1] ?? $filename;

    $collection = new Collection(
      self::$_known_xpath,
      /** Fonction d'identification de la valeur de tri */
      function(string $index, array $item): string { return $index; },
      /** Fonction de comparaison pour le tri */
      function(string $keyA, string $keyB): bool { return (strcmp($keyA,$keyB) < 0); }
    );
    $collection->heapSort();
    $output = [
      '<?xml version="1.0" encoding="ISO-8859-1" ?>',
      "<!-- DTD AUTOGENERE LE $strNow du fichier '$filename' -->",
    ];

    foreach($collection as $index => $data)
      $output[]=self::convert_array_to_string($data);

    return implode("\r\n", $output);
  }
  /**
   * Fourniture du template par défaut d'un nouvel élément
   *
   * @param string $xpath
   * @param string $elementName
   * @param bool $isValue
   * @param bool $nullable
   * @return array
   */
  private static function generate_new_element(
    string $xpath,
    string $elementName,
    bool $isValue=false,
    bool $nullable=false
  ): array {
    return [
      'xpath' => $xpath,
      'element' => $elementName,
      'is_value' => $isValue,
      'nullable' => $nullable,
      'associations' => [
        'ZeroToOne' => [],
        'StrictlyOne' => [],
        'OneToMany' => [],
        'ZeroToMany' => [],
      ],
    ];
  }

  /**
   * Ajout ou mise à jour d'un attribut dans l'élément
   *
   * @param string $xpath
   * @param string $elementName
   * @param \SimpleXMLElement $element
   * @return void
   */
  private static function insert_or_update_attribute(string $xpath,string $elementName, \SimpleXMLElement $element): void {
    $val = ((string)$element === "");
    $tmp= self::$_known_xpath[$xpath] ?? self::generate_new_element(xpath: $xpath, elementName: $elementName, isValue: true, nullable: $val);
    $isNullable = $tmp['nullable'];
    $tmp['nullable'] = (false === $isNullable) ? $val : $isNullable;
    self::$_known_xpath[$xpath]=$tmp;
  }

  /**
   * Insertion ou mise à jour d'une association à un élément
   *
   * @param string $xpath
   * @param string $association
   * @param bool $isMultiple
   * @return void
   */
  private static function insert_or_update_association(
    string $xpath,
    string $association,
    bool $isMultiple=false
  ): void {
    $tmp = self::$_known_xpath[$xpath];
    /** @var bool $check Contrôle que l'association n'est pas déclarée */
    $associationTypes = ['ZeroToOne', 'StrictlyOne', 'ZeroToMany', 'OneToMany'];
    $check = true;
    foreach($associationTypes as $associationType)
      $check = $check && !in_array($association, $tmp['associations'][$associationType]);

    if(true === $check)
      self::$_known_xpath[$xpath]['associations']['StrictlyOne'][]=$association;

    if(true === $isMultiple){
      $associationTypes = ['StrictlyOne' => 'OneToMany', 'ZeroToOne' => 'ZeroToMany'];
      foreach($associationTypes as $oldType => $newType) {
        /** @var int|bool $target */
        $target = array_search($association, self::$_known_xpath[$xpath]['associations'][$oldType]);
        if(false !== $target) {
          self::$_known_xpath[$xpath]['associations'][$newType][]=$association;
          unset(self::$_known_xpath[$xpath]['associations'][$oldType][$target]);
        }
      }
    }
  }

  /**
   * Fonction globale de traitement d'un noeud XML
   *
   * @param string $xpath
   * @param string $elementName
   * @param \SimpleXMLElement $element
   * @return void
   */
  private static function launch_child_sub_information(string $xpath, string $elementName, \SimpleXMLElement $element): void {
    self::$_known_xpath[$xpath]=self::$_known_xpath[$xpath] ?? self::generate_new_element($xpath, $elementName);

    /**
     * Etape 1 : traitement des attributs associés à un élément
     *
     */
    /** @var \SimpleXMLElement $attrs Réduction de l'élément à ses attributs */
    $attrs = $element->attributes();
    /**
     * @var string $attrName Nom de l'attribut
     * @var \SimpleXMLElement $attrValue Réduction de l'attribut à sa valeur
     */
    foreach($attrs as $attrName => $attrValue) {
      self::insert_or_update_attribute("$xpath/$attrName", $attrName, $attrValue);
      self::insert_or_update_association($xpath, $attrName);
    }

    $children = $element->children();

    if(count($children)) {

      /**
      * Etape 2.1 : traitement des noeuds enfants associés à un élément
      *
      */
      /** @var array $tblNodeName Tableau de détection des multiplicité de noeuds enfants */
      $tblNodeName=[];
      foreach($element->children() as $nodeName => $child) {
        $tblNodeName[$nodeName] = isset($tblNodeName[$nodeName]);
        self::launch_child_sub_information(xpath: "$xpath/$nodeName",elementName: $nodeName, element: $child);
        self::insert_or_update_association($xpath, $nodeName, $tblNodeName[$nodeName]);
      }
      /**
       * Etape 2.2 : traitement des noeuds enfants manquants par rapport à un élément
       *
       */
      self::perform_consistency_associations($xpath, $tblNodeName);

      unset($tblNodeName);

    }
    else {
      self::$_known_xpath[$xpath]['is_value']=true;
      $val = ((string)$element === "");
      $isNullable = self::$_known_xpath[$xpath]['nullable'];
      self::$_known_xpath[$xpath]['nullable'] = (false === $isNullable) ? $val : $isNullable;
    }
  }

  /**
   * Détection des associations pouvant ne pas être présente
   *
   * @param string $xpath
   * @param array $nodeNames
   * @return void
   */
  private static function perform_consistency_associations(string $xpath, array $nodeNames): void {
    /** @var array $associationTypes Types d'association candidats à être déplacé vers le Zero... */
    $associationTypes = ['StrictlyOne' => 'ZeroToOne', 'OneToMany' => 'ZeroToMany'];
    foreach($associationTypes as $associationOldType => $associationNewType) {
      $associations = self::$_known_xpath[$xpath]['associations'][$associationOldType];
      foreach($associations as $association) {
        if(!in_array($association, array_keys($nodeNames))) {
          /** @var int $index */
          $index = array_search($association, $associations);
          unset(self::$_known_xpath[$xpath]['associations'][$associationOldType][$index]);
          self::$_known_xpath[$xpath]['associations'][$associationNewType][]=$association;
        }
      }
    }
  }
}
