<?php

namespace FOPG\Component\MOFBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use FOPG\Component\MOFBundle\Entity\UnityClass;

/**
 * @extends ServiceEntityRepository<UnityClass>
 *
 * @method UnityClass|null find($id, $lockMode = null, $lockVersion = null)
 * @method UnityClass|null findOneBy(array $criteria, array $orderBy = null)
 * @method UnityClass[]    findAll()
 * @method UnityClass[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UnityClassRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UnityClass::class);
    }

    public function save(UnityClass $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(UnityClass $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return UnityClass[] Returns an array of UnityClass objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('u.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?UnityClass
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
