<?php

namespace FOPG\Component\MOFBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use FOPG\Component\MOFBundle\Entity\UnityNamespace;

/**
 * @extends ServiceEntityRepository<UnityNamespace>
 *
 * @method UnityNamespace|null find($id, $lockMode = null, $lockVersion = null)
 * @method UnityNamespace|null findOneBy(array $criteria, array $orderBy = null)
 * @method UnityNamespace[]    findAll()
 * @method UnityNamespace[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UnityNamespaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UnityNamespace::class);
    }

    public function save(UnityNamespace $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(UnityNamespace $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return UnityNamespace[] Returns an array of UnityNamespace objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('u.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?UnityNamespace
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
