<?php

namespace FOPG\Component\MOFBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use FOPG\Component\MOFBundle\Entity\UnityAttribute;

/**
 * @extends ServiceEntityRepository<UnityAttribute>
 *
 * @method UnityAttribute|null find($id, $lockMode = null, $lockVersion = null)
 * @method UnityAttribute|null findOneBy(array $criteria, array $orderBy = null)
 * @method UnityAttribute[]    findAll()
 * @method UnityAttribute[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UnityAttributeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UnityAttribute::class);
    }

    public function save(UnityAttribute $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(UnityAttribute $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return UnityAttribute[] Returns an array of UnityAttribute objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('u.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?UnityAttribute
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
