<?php

namespace FOPG\Component\MOFBundle\Exception\Dtd;

class InvalidOperatorException extends \Exception
{
	public function __construct($operator,$code=404)
	{
		parent::__construct("L'opérateur DTD ".$operator." n'existe pas!",$code);
	}
}
