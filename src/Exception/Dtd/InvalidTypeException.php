<?php

namespace FOPG\Component\MOFBundle\Exception\Dtd;

class InvalidTypeException extends \Exception
{
	public function __construct($type,$code=404)
	{
		parent::__construct("Le type DTD ".$type." n'existe pas!",$code);
	}
}
