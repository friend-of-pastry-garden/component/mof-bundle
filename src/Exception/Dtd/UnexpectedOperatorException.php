<?php

namespace FOPG\Component\MOFBundle\Exception\Dtd;

class UnexpectedOperatorException extends \Exception
{
	public function __construct($operator,$code=404)
	{
		parent::__construct("L'opérateur DTD ".$operator." n'est pas prévu dans ce contexte!",$code);
	}
}
