<?php

namespace FOPG\Component\MOFBundle\Exception\Unity;

use FOPG\Component\MOFBundle\Serializer\Unity\UnityAttribute;

class InconsistencyAttributeException extends \Exception
{
	public function __construct(UnityAttribute $a, UnityAttribute $b,$code=404)
	{
		/** @var string $aName */
		$aName = $a->getName();
		/** @var string $bName */
		$bName = $b->getName();
		parent::__construct("Les deux attributs n'ont pas la même règle de nommage : $aName et $bName détecté !",$code);
	}
}
