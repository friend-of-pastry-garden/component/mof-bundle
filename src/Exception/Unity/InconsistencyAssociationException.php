<?php

namespace FOPG\Component\MOFBundle\Exception\Unity;

use FOPG\Component\MOFBundle\Serializer\Unity\UnityAssociation;

class InconsistencyAssociationException extends \Exception
{
	public function __construct(UnityAssociation $a, UnityAssociation $b,$code=404)
	{
		/** @var string $aName */
		$aName = $a->getName();
		/** @var string $bName */
		$bName = $b->getName();
		parent::__construct("Les deux associations n'ont pas la même règle de nommage : $aName et $bName détecté !",$code);
	}
}
