<?php

namespace FOPG\Component\MOFBundle\Exception\Unity;

use FOPG\Component\MOFBundle\Serializer\Unity\UnityAssociationEnd;

class InconsistencyAssociationEndException extends \Exception
{
	public function __construct(UnityAssociationEnd $a, UnityAssociationEnd $b,$code=404)
	{
		/** @var string $aName */
		$aName = $a->getName();
		/** @var string $bName */
		$bName = $b->getName();
		parent::__construct("Les deux associations terminales n'ont pas la même règle de nommage : $aName et $bName détecté !",$code);
	}
}
