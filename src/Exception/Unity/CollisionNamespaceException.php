<?php

namespace FOPG\Component\MOFBundle\Exception\Unity;

use FOPG\Component\MOFBundle\Serializer\Unity\UnityNamespace;

class CollisionNamespaceException extends \Exception
{
	public function __construct(UnityNamespace $a, UnityNamespace $b,$code=404)
	{
		/** @var string $aName */
		$aName = $a->getName();
		/** @var string $bName */
		$bName = $b->getName();
		parent::__construct("Il n'est pas possible d'associer deux espaces de noms distincts : $aName et $bName détecté !",$code);
	}
}
