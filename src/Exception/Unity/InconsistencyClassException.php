<?php

namespace FOPG\Component\MOFBundle\Exception\Unity;

use FOPG\Component\MOFBundle\Serializer\Unity\UnityClass;

class InconsistencyClassException extends \Exception
{
	public function __construct(UnityClass $a, UnityClass $b,$code=404)
	{
		/** @var string $aName */
		$aName = $a->getName();
		/** @var string $bName */
		$bName = $b->getName();
		parent::__construct("Les deux classes n'ont pas la même règle de nommage : $aName et $bName détecté !",$code);
	}
}
