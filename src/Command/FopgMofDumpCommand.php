<?php

namespace FOPG\Component\MOFBundle\Command;
use FOPG\Component\MOFBundle\Contracts\Unity\UnityAssociationEndInterface;
use FOPG\Component\MOFBundle\Serializer\DtdSerializer;
use FOPG\Component\MOFBundle\Serializer\PdfSerializer;
use FOPG\Component\MOFBundle\Serializer\XmlSerializer;
use FOPG\Component\MOFBundle\Serializer\Unity\UnityAttribute;
use FOPG\Component\MOFBundle\Serializer\Unity\UnityAssociationEnd;
use FOPG\Component\MOFBundle\Serializer\Unity\UnityClass;
use FOPG\Component\UtilsBundle\Command\AbstractCommand;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

#[AsCommand(
    name: 'fopg:mof:dump',
    description: "Outil d'impression pour visualiser les associations trouvées dans le fichier source",
)]
class FopgMofDumpCommand extends AbstractCommand
{
    public function __construct(ContainerInterface $container) {
        parent::__construct($container);
    }

    protected function configure(): void
    {
        parent::configure();
        $this
            ->addArgument('file', InputArgument::REQUIRED, 'Fichier fournissant la description du modèle à considérer')
            ->addOption('namespace', null, InputOption::VALUE_OPTIONAL, 'Espace de nom associé au fichier')
        ;
    }

    /**
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        parent::execute($input, $output);

        /** @var string $file */
        $file     = $input->getArgument('file');
        /** @var ?string $namespace */
        $namespace= $input->getOption('namespace',null);
        if(file_exists($file)) {
          if(preg_match("/[.](?<extension>\w+)$/", $file, $matches)) {
            switch(strtolower($matches['extension'])) {
              case 'dtd':
                $obj = new DtdSerializer(filename: $file, namespace: $namespace);
                break;
              case 'xml':
                $xml = new XmlSerializer(filename: $file);
                $tmpFile = XmlSerializer::generate_dtd_from_xml($file);
                $obj = new DtdSerializer(filename: $tmpFile, namespace: $namespace ?? $file);
                XmlSerializer::guess_types($xml, $obj);
                break;
              case 'pdf':
                $pdf = new PdfSerializer($file);
                $associatedFiles = $pdf->getAssociatedFiles();
                $xml = null;
                $tmpFile = null;
                foreach($associatedFiles as $associatedFile) {
                  if(XmlSerializer::isSerializable($associatedFile)) {
                    $xml = new XmlSerializer(filename: $associatedFile, namespace: $namespace ?? $file);
                    $tmpFile = XmlSerializer::generate_dtd_from_xml($associatedFile);
                  }
                }
                if($tmpFile) {
                  $obj = new DtdSerializer(filename: $tmpFile, namespace: $namespace ?? $file);
                  XmlSerializer::guess_types($xml, $obj);
                }
                else {
                  $this->unlock();
                  return 0;
                }

                break;
              default:
                $this->unlock();
                return 0;
            }
          }

          /** @var UnityClass $root */
          $root = $obj->getRoot();
          $this->analyze_new_class($root);
        }

        $this->unlock();
        return Command::SUCCESS;
    }

    private static function print_attribute(UnityAttribute $attr): string {
      $type = ($attr->isNullable() ? '?' : '').$attr->getType();
      if($attr::TYPE_STRING == $attr->getType()) {
        $maxlength = $attr->getOption('maxlength') ?? $attr::MAXLENGTH_STRING;
        $type.="(".$maxlength.")";
      }
      if($attr::TYPE_ENUM == $attr->getType()) {
        $choices = $attr->getOption('choices') ?? [];
        $type.="('".implode("','", $choices)."')";
      }
      $primaryKey = $attr->isPartOfHash() ? '#' : '';
      return " $type ".$primaryKey.$attr->getSmallName();
    }

    private static function print_class(UnityClass $class, ?string $multiplicity=null, ?UnityAssociationEnd $assocEnd=null): string {
      $prefix = "";
      $suffix = "";
      switch($multiplicity) {
        case UnityAssociationEndInterface::ONE_TO_ONE:
          break;
        case UnityAssociationEndInterface::ZERO_TO_ONE:
          $prefix="?".$prefix;
          break;
        case UnityAssociationEndInterface::ZERO_TO_MANY:
        case UnityAssociationEndInterface::ONE_TO_MANY:
          $prefix="Collection<";
          $suffix=">";
          break;
        default:
      }

      $namespace = $class->getNamespace() ? $class->getNamespace()->getName().'/' : "";
      return " ".$prefix.$namespace.$class->getName().$suffix." ".($assocEnd ? $assocEnd->getName() : $class->getName());
    }

    private function analyze_new_class(UnityClass $class, ?string $multiplicity=null, int $depth=0, ?UnityAssociationEnd $assocEnd=null): void {
      $name = self::print_class($class, $multiplicity, $assocEnd);
      $output = [];

      $attrs = $class->getAttributes();
      /** @var array<int, UnityAssociationEnd> $assocEnds */
      $assocEnds = $class->getAssociationRgtEnds();
      foreach($attrs as $attr) {
        $output[self::print_attribute($attr)]=1
        ;
      }
      $this->logTable([$name => 0], $depth*3);
      foreach($assocEnds as $assocEnd) {
        $childClass = $assocEnd
          ->getAssociation()
          ->getAssociationRgt()
          ->getClass()
        ;
        $multiplicity = $assocEnd
          ->getAssociation()
          ->getAssociationRgt()
          ->getMultiplicity()
        ;

        $this->analyze_new_class($childClass, $multiplicity, $depth+1, $assocEnd);
      }
      $this->logTable($output, ($depth+1)*3);
    }
}
