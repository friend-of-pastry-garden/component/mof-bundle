<?php

namespace FOPG\Component\MOFBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

trait UnityPropertyVersionTrait
{
  #[ORM\Column]
  private ?int $version = null;

  public function getVersion(): ?int
  {
      return $this->version;
  }

  public function setVersion(int $version): self
  {
      $this->version = $version;

      return $this;
  }
}
