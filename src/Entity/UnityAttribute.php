<?php

namespace FOPG\Component\MOFBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use FOPG\Component\MOFBundle\Repository\UnityAttributeRepository;

#[ORM\Entity(repositoryClass: UnityAttributeRepository::class)]
class UnityAttribute
{
    use UnityPropertyNameTrait;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $fullName = null;

    #[ORM\Column(length: 20)]
    private ?string $type = null;

    #[ORM\Column(type: Types::ARRAY)]
    private array $options = [];

    #[ORM\Column]
    private ?bool $nullable = null;

    #[ORM\Column]
    private ?bool $partOfHash = null;

    #[ORM\ManyToOne(inversedBy: 'attributes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?UnityClass $class = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): self
    {
        $this->options = $options;

        return $this;
    }

    public function isNullable(): ?bool
    {
        return $this->nullable;
    }

    public function setNullable(bool $nullable): self
    {
        $this->nullable = $nullable;

        return $this;
    }

    public function isPartOfHash(): ?bool
    {
        return $this->partOfHash;
    }

    public function setPartOfHash(bool $partOfHash): self
    {
        $this->partOfHash = $partOfHash;

        return $this;
    }

    public function getClass(): ?UnityClass
    {
        return $this->class;
    }

    public function setClass(?UnityClass $class): self
    {
        $this->class = $class;

        return $this;
    }
}
