<?php

namespace FOPG\Component\MOFBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOPG\Component\MOFBundle\Repository\UnityNamespaceRepository;

#[ORM\Entity(repositoryClass: UnityNamespaceRepository::class)]
class UnityNamespace
{
    use UnityPropertyNameTrait;
    use UnityPropertyVersionTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToMany(mappedBy: 'namespace', targetEntity: UnityClass::class)]
    private Collection $classes;

    public function __construct()
    {
        $this->classes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, UnityClass>
     */
    public function getClasses(): Collection
    {
        return $this->classes;
    }

    public function addClass(UnityClass $class): self
    {
        if (!$this->classes->contains($class)) {
            $this->classes->add($class);
            $class->setNamespace($this);
        }

        return $this;
    }

    public function removeClass(UnityClass $class): self
    {
        if ($this->classes->removeElement($class)) {
            // set the owning side to null (unless already changed)
            if ($class->getNamespace() === $this) {
                $class->setNamespace(null);
            }
        }

        return $this;
    }
}
