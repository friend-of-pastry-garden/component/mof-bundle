<?php

namespace FOPG\Component\MOFBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOPG\Component\MOFBundle\Repository\UnityClassRepository;

#[ORM\Entity(repositoryClass: UnityClassRepository::class)]
class UnityClass
{
    use UnityPropertyNameTrait;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'classes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?UnityNamespace $namespace = null;

    #[ORM\OneToMany(mappedBy: 'class', targetEntity: UnityAttribute::class)]
    private Collection $attributes;

    public function __construct()
    {
        $this->attributes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNamespace(): ?UnityNamespace
    {
        return $this->namespace;
    }

    public function setNamespace(?UnityNamespace $namespace): self
    {
        $this->namespace = $namespace;

        return $this;
    }

    /**
     * @return Collection<int, UnityAttribute>
     */
    public function getAttributes(): Collection
    {
        return $this->attributes;
    }

    public function addAttribute(UnityAttribute $attribute): self
    {
        if (!$this->attributes->contains($attribute)) {
            $this->attributes->add($attribute);
            $attribute->setClass($this);
        }

        return $this;
    }

    public function removeAttribute(UnityAttribute $attribute): self
    {
        if ($this->attributes->removeElement($attribute)) {
            // set the owning side to null (unless already changed)
            if ($attribute->getClass() === $this) {
                $attribute->setClass(null);
            }
        }

        return $this;
    }
}
