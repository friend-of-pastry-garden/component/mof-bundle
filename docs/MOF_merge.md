Introduction
--
Nous avons étudier l'acquisition de la représentation d'un modèle par la commande **fopg:mof:dump**. Nous allons maintenant étudier la méthode pour mettre à jour la représentation de modèle par la méthode de fusion.

Présentation générale
--
Le processus de fusion s'opère par une commande CLI à partir d'une source de donnée vers nom de domaine.

```
php bin/console fopg:mof:merge <source de donnée> <namespace> [--method=<method>] [--force]
```

La commande sert à la mise à jour de l'espace de nom. Selon la méthode appelée (hard ou soft), un ensemble de processus vont piloter les mises à jour expliquée plus bas. A noter que l'absence de l'option **force** ne fera qu'un affichage des mises à jour à opérer. Son enregistrement nécessite l'ajout de la commande **force**.

### Description des méthodes de fusion
Selon l'objectif désiré, la méthode peut varier. Ci-dessous sont répertoriés les différentes stratégies et les incidences :

#### La méthode hard

La méthode hard remplace la version courante du nom de domaine par la représentation trouvée dans la source de donnée.

**Exemple d'utilisation de la méthode hard**
L'exemple typique est lorsque l'espace de nom a été peuplé grâce à une DTD qui est très peu précise. Dès lors que l'on dispose du XML de référence, alors on appelera la commande en mode **hard** pour consolider les informations de la représentation.

#### La méthode soft

La méthode soft va procéder à une fusion entre la source de donnée et la version courante de l'espace de nom. La fusion va ensuite être analysée et va soit engendrer une nouvelle version, soit mettre à jour certaines informations de la version courante.

**Exemple d'utilisation de la méthode soft**
L'exemple typique est lorsque l'espace de nom a été peuplé grâce à un premier XML et qu'un nouveau XML de mise à jour a été mis à disposition.

#### Résultat attendu de l'opération de fusion

**Impact sur le type d'un attribut**

horizontal : type détecté
vertical   : type connu dans la source

|             | enum    | integer | date    | varchar | text |
| --          | --      | --      | --      | --      | --   |
| **enum**    | enum    | varchar | varchar | varchar | text |
| **integer** | varchar | integer | varchar | varchar | text |
| **date**    | varchar | varchar | date    | varchar | text |
| **varchar** | varchar | varchar | varchar | varchar | text |
| **text**    | text    | text    | text    | text    | text |

**Impact sur la nullité d'un attribut**
L'attribut est marqué comme éligible à la nullité si la source ou le nouveau modèle ont l'information de nullité valide.

**Impact d'une mise à jour sur les options**
Les options dans leur ensemble sont écrasés par la nouvelle source de donnée. Seule l'information **maxlength** est soumise à contrôle en prenant la valeur maximale entre la source et le nouveau modèle.

**Impact d'une différence dans l'arité d'une classe**
L'arité maximale prend l'ascendant dans le résultat.

**Impact de la modification de participation à la clé de hashage d'un attribut**
La modification de participation à la clé de hashage peut provenir soit d'une action manuelle, soit d'une détection de nullité sur l'attribut considéré ou encore de l'ajout d'un nouvel attribut.

#### Comportement attendu du script sous méthode soft
* Si aucune modification n'a été détecté après fusion, alors aucune mise à jour n'est opérée.
* Si au moins une modification de participation est détecté après fusion, alors cela entraînera la génération d'une nouvelle version du nom de domaine.
* Si au moins une mise à jour a été détecté autre que la participation, alors le modèle courant sera mis à jour sur la version courante.
