Introduction
--
L'objectif premier du MOF est d'extraire le méta-modèle d'une source de donnée. Ce chapitre
s'articulera sur la procédure d'effectuer cette démarche.

Présentation générale
--
Le processus d'extraction s'opère par une commande CLI à partir d'une source de donnée.

```
php bin/console fopg:mof:dump <source de donnée> [--namespace=<nom d'espace>] [--save]
```

La commande va étudier chaque segment de la source et produire les associations/attributs correspondants. Une fois cela réalisé, elle peut optionnellement mémoriser l'ensemble grâce à l'espace de nom correspondant. A noter que la sauvegarde sera interdite s'il y'a déjà un modèle sauvegardé avec cet espace de nom (cf la commande fopg:mof:merge)

A l'issue du processus, la commande produit un arbre résumant le résultat.

![](fopg_mof_dump_display.png)

### Description d'un embranchement d'arbre

On appelle **feuille** tout élément de l'arbre qui n'a pas de sous-élément associé. Tout ce qui n'est pas feuille se nomme **noeud**. La racine de l'arbre commence à l'extrêmum gauche, la classe **Root**, puis redescend récursivement en dépilant les associations à droite.

#### Composition d'une feuille

Chaque feuille correspond à un attribut. La syntaxe de représentation respecte la règle suivante :

```
[{nullable}]{type} [{partOfHash}]{attributeName}
```

Avec :

| variable | description | comportement |
| -- | -- | -- |
| nullable | L'attribut autorise t'il des valeurs vide ? | Si oui nullable affichera '?' |
| type | type de l'attribut | <cf. liste des types ci-après |
| partOfHash | l'attribut sert t'il à produire le hash de l'instance de classe ? | Si oui, partOfHash affichera '#' |
| attributeName | nom de l'attribut | Le nom de l'attribut se conforme au format lowerCamelCase |

Les types de Unity sont les suivants :

| type | description | exemple |
| -- | -- | -- |
| varchar(L) | chaîne de caractère de longueur maximale L | varchar(50) |
| date | date | date |
| enum(Z) | Enumération dont le tableau Z contient l'intégralité des membres | enum('O','N') |
| integer | entier | integer |
| text | chaîne de caractère supérieure à 255 caractères | text |

#### Composition d'un noeud

Chaque noeud correspond à une classe où une collection de classe. L'arité de droite de l'association va déterminer sa nature. La syntaxe de représentation respecte la règle suivante :

##### cas 1: l'arité est de type 0..1 ou 1..1
```
[{nullable}]{className}
```

##### cas 2 : l'arité est de type 0..*
```
Collection<{className}>
```

Avec :

| variable | description | comportement |
| -- | -- | -- |
| nullable | une instance de classe doit-elle obligatoirement être présente ? | Si oui, nullable affichera '?' |
| className | nom de l'instance de classe | l'instance de classe se conforme au format upperCamelCase |
